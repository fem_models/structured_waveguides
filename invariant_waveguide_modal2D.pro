Include "parameters_gmsh_getdp.dat";
// Include "test.txt";

Group {
  Subs        = Region[1];
  Low         = Region[2];
  Guide       = Region[3];
  Super       = Region[4];
  PMLsubs_x   = Region[5];
  PMLsubs_y   = Region[6];
  PMLsubs_xy  = Region[7];
  PMLlow_x    = Region[8];
  PMLsuper_x  = Region[9];
  PMLsuper_y  = Region[10];
  PMLsuper_xy = Region[11];	
  Surf_sym_x0 = Region[12];
  Bord_Dirichlet_outer      = Region[13];
  Point_guide_middle_bottom = Region[14];
  PML_xy        = Region[{PMLsubs_xy,PMLsuper_xy}];
  PML_x         = Region[{PMLsubs_x,PMLlow_x,PMLsuper_x}];
  PML_y         = Region[{PMLsubs_y,PMLsuper_y}];
  PMLs   = Region[{PML_xy,PML_x,PML_y}];
  Domain = Region[{Subs,Low,Guide,Super}];
  Omega  = Region[{Domain,PMLs}];
}


Function {
  I[]        = Complex[0,1];    //guilladd
  EZ[] = Vector[0.0,0.0,1.0];   // used in the formulation
  eps_subs[]  = Complex[eps_subs,0];
  eps_low[]   = Complex[eps_low,0];
  eps_guide[] = Complex[eps_guide,0];
  eps_super[] = Complex[eps_super,0];
  lambda0    = lambda0*um;  // longueur d onde , attention au facteur "um" par rapport à fibre-ms
  Freq       = cel/lambda0;
  omega0     = 2.*Pi*cel/lambda0; // attention au facteur "cel" par rapport à fibre-ms
  k0         = 2.*Pi/lambda0;
  omega2[]   = k0^2; // BEWARE , NO "cel" factor here
  n = 0;
  sEl=1.; // debug
	
  a_pml            =  1.;
  b_pml            = siwt*1.;
  sx[Domain]       = 1.;
  sy[Domain]       = 1.;
  sz[Domain]       = 1.;
  sx[PML_xy]       = Complex[a_pml,-b_pml];
  sy[PML_xy]       = Complex[a_pml,-b_pml];
  sz[PML_xy]       = 1.0;
  sx[PML_x]        = Complex[a_pml,-b_pml];
  sy[PML_x]        = 1.0;
  sz[PML_x]        = 1.0;
  sx[PML_y]        = 1.0;
  sy[PML_y]        = Complex[a_pml,-b_pml];
  sz[PML_y]        = 1.0;
  Lxx[]            = sy[]*sz[]/sx[];
  Lyy[]            = sz[]*sx[]/sy[]; 
  Lzz[]       	   = sx[]*sy[]/sz[];
  PML_tens[]       = TensorDiag[Lxx[],Lyy[],Lzz[]];

  epsilonr[PMLsubs_x]   = eps_subs[]  * PML_tens[];
  epsilonr[PMLsubs_y]   = eps_subs[]  * PML_tens[];
  epsilonr[PMLsubs_xy]  = eps_subs[]  * PML_tens[];
  epsilonr[PMLlow_x]    = eps_low[]   * PML_tens[];
  epsilonr[PMLsuper_x]  = eps_super[] * PML_tens[];
  epsilonr[PMLsuper_y]  = eps_super[] * PML_tens[];
  epsilonr[PMLsuper_xy] = eps_super[] * PML_tens[];
  epsilonr[Subs]        = eps_subs[]  * TensorDiag[1,1,1];
  epsilonr[Low]         = eps_low[]   * TensorDiag[1,1,1];
  epsilonr[Guide]       = eps_guide[] * TensorDiag[1,1,1];
  epsilonr[Super]       = eps_super[] * TensorDiag[1,1,1];

  mur[PMLsubs_x]    = TensorDiag[Lxx[],Lyy[],Lzz[]];
  mur[PMLsubs_y]    = TensorDiag[Lxx[],Lyy[],Lzz[]];
  mur[PMLsubs_xy]   = TensorDiag[Lxx[],Lyy[],Lzz[]];
  mur[PMLlow_x]     = TensorDiag[Lxx[],Lyy[],Lzz[]];
  mur[PMLsuper_x]   = TensorDiag[Lxx[],Lyy[],Lzz[]];
  mur[PMLsuper_y]   = TensorDiag[Lxx[],Lyy[],Lzz[]];
  mur[PMLsuper_xy]  = TensorDiag[Lxx[],Lyy[],Lzz[]];
  mur[Subs]         = TensorDiag[1,1,1];
  mur[Low]          = TensorDiag[1,1,1];
  mur[Guide]        = TensorDiag[1,1,1];
  mur[Super]        = TensorDiag[1,1,1];

  epsilonr_annex[PMLsubs_x]   = epsilonr[];
  epsilonr_annex[PMLsubs_y]   = epsilonr[];
  epsilonr_annex[PMLsubs_xy]  = epsilonr[];
  epsilonr_annex[PMLlow_x]    = epsilonr[];
  epsilonr_annex[PMLsuper_x]  = epsilonr[];
  epsilonr_annex[PMLsuper_y]  = epsilonr[];
  epsilonr_annex[PMLsuper_xy] = epsilonr[];
  epsilonr_annex[Subs]        = epsilonr[];
  epsilonr_annex[Low]         = epsilonr[];
  epsilonr_annex[Guide]       = eps_super[];
  epsilonr_annex[Super]       = epsilonr[];

  ui[]       = Complex[ Cos[k0*Y[]] , Sin[k0*Y[]] ];
  source_i[] = (omega0/cel)^2*(epsilonr[]-epsilonr_annex[])*ui[]; // a "cel" factor is used here 
}

Constraint {
  { Name arete;
    Case {
      { Region Bord_Dirichlet_outer; Value 0. ;}
      If(flag_sym_diri)
        { Region Surf_sym_x0 ; Value 0. ;}
      EndIf
	}
  }
  { Name arete_2E;
    Case {
      { Region Bord_Dirichlet_outer; Value 0. ;}
      If(flag_sym_diri)
        { Region Surf_sym_x0 ; Value 0. ;}
      EndIf
	}
  }
  { Name nodal;
    Case {
      { Region Bord_Dirichlet_outer; Value 0. ;}
      If(flag_sym_diri)
        { Region Surf_sym_x0 ; Value 0. ;}
      EndIf
	}
  }
  { Name nodal_2E;
    Case {
      { Region Bord_Dirichlet_outer; Value 0. ;}
      If(flag_sym_diri)
        { Region Surf_sym_x0 ; Value 0. ;}
      EndIf
	}
  }
}

Jacobian {
  { Name JVol ;
    Case { 
      { Region All ; Jacobian Vol ; }
    }
  }
  { Name JSur ;
    Case {
      { Region All ; Jacobian Sur ; }
    }
  }
}

Integration {
  { Name Int_1 ;
    Case { 
      { Type Gauss ;
        Case { 
          { GeoElement Point       ; NumberOfPoints  1 ; }
          { GeoElement Line        ; NumberOfPoints  4 ; }
          { GeoElement Triangle    ; NumberOfPoints  12 ; }
        }
      }
    }
  }
}



FunctionSpace { 
  
  { Name E_arete; Type Form1; 
    BasisFunction { 
      { Name se ; NameOfCoef he_i   ; Function BF_Edge; Support Omega; Entity EdgesOf[All]; }
      { Name se2E; NameOfCoef he2E_i; Function BF_Edge_2E; Support Omega; Entity EdgesOf[All]; } 
    } 
    Constraint { 
      { NameOfCoef he_i   ; EntityType EdgesOf ; NameOfConstraint arete; } 
      { NameOfCoef he2E_i ; EntityType EdgesOf ; NameOfConstraint arete_2E; } 
    } 
  } 
  { Name E_arete_perp; Type Form1P; 
    BasisFunction { 
      { Name sn; NameOfCoef hn_i; Function BF_PerpendicularEdge; Support Omega; Entity NodesOf[All]; } 
      { Name sn2E; NameOfCoef hn2E_i; Function BF_PerpendicularEdge_2E; Support Omega; Entity EdgesOf[All]; } /* pour avoir les elts d ordre 2 */
    } 
    Constraint { 
      { NameOfCoef hn_i; EntityType NodesOf ; NameOfConstraint nodal; } 
      { NameOfCoef hn2E_i; EntityType EdgesOf ; NameOfConstraint nodal_2E; } 
    } 
  } 
  
}
 

Formulation {
  { Name Guide_e_2D_G; Type FemEquation;
    Quantity {  
      { Name Et; Type Local; NameOfSpace E_arete; } 
      { Name El; Type Local; NameOfSpace E_arete_perp; } 
    } 
    Equation { 
      Galerkin {        [ 1./mur[]            * Dof{d Et}         , {d Et}     ]; In Omega; Integration Int_1; Jacobian JVol; } // Att
      Galerkin {        [-omega2[]*epsilonr[] * Dof{Et}           , {Et}       ]; In Omega; Integration Int_1; Jacobian JVol; } // Ctt
      Galerkin { DtDtDof[-1./mur[]            * (EZ[] *^ Dof{Et}) , EZ[]*^{Et} ]; In Omega; Integration Int_1; Jacobian JVol; } // Btt
      Galerkin { DtDtDof[-1./mur[]            * Dof{d El}         , EZ[]*^{Et} ]; In Omega; Integration Int_1; Jacobian JVol; } // Azt
      Galerkin { DtDtDof[-1./mur[]            * (EZ[] *^ Dof{Et}) , {d El}     ]; In Omega; Integration Int_1; Jacobian JVol; } // Atz
      Galerkin { DtDtDof[-1./mur[]            * Dof{d El}         , {d El}     ]; In Omega; Integration Int_1; Jacobian JVol; } // Azz
      Galerkin { DtDtDof[ omega2[]*epsilonr[] * Dof{El}           , {El}       ]; In Omega; Integration Int_1; Jacobian JVol; } // Czz
    } 
  }
} 

Resolution {  
  { Name Guide_e_2D_G_PVP;
    System {
      { Name A; NameOfFormulation Guide_e_2D_G; Type ComplexValue; }
	
    } 
    Operation {  
      GenerateSeparate[A]; 
      EigenSolve[A,neig,shift_lin_re,shift_lin_im]; 
      PostOperation[SaveEigen_eG];
      PostOperation[SaveField_eG];
    } 
  } 
}

PostProcessing {  

  { Name Guide_e_2D_G; NameOfFormulation Guide_e_2D_G; NameOfSystem A;
    Quantity { 
      { Name et; Value{ Local{ [ {Et} ] ; In Omega; Integration Int_1; Jacobian JVol; } } } 
      { Name el; Value{ Local{ [ sEl*I[]*$Eigenvalue* {El} ] ; In Omega; Integration Int_1; Jacobian JVol; } } }

      { Name enorm ; Value{ Local{ [ Norm[{Et}+sEl*I[]*$Eigenvalue* {El}  ]  ]  ; In Omega; Integration Int_1; Jacobian JVol; } } }
      { Name efull    ; Value{ Local{ [ {Et}+sEl*I[]*$Eigenvalue* {El} ]     ; In Omega; Integration Int_1; Jacobian JVol; } } }
      { Name epsrefull; Value{ Local{ [ epsilonr[]*({Et}+sEl*I[]*$Eigenvalue* {El}) ]     ; In Omega; Integration Int_1; Jacobian JVol; } } }
      { Name hfull    ; Value{ Local{ [ siwt*I[]/(mur[]*mu0*omega0)*norm_dint*(
                                      	{d Et}
                                      	+sEl*I[]*$Eigenvalue* {d El}
                                      	+I[]*sigz*Complex[$EigenvalueReal,$EigenvalueImag]*Vector[-CompY[{Et}],CompX[{Et}],0.]
                                      		) ] ; In Omega; Integration Int_1; Jacobian JVol; } } }
      // debug H
      { Name hpart1    ; Value{ Local{ [{d Et}]; In Omega; Integration Int_1; Jacobian JVol; } } }
      { Name hpart2    ; Value{ Local{ [sEl*I[]*$Eigenvalue* {d El}]; In Omega; Integration Int_1; Jacobian JVol; } } }
      { Name hpart3    ; Value{ Local{ [I[]*sigz*Complex[$EigenvalueReal,$EigenvalueImag]*Vector[-CompY[{Et}],CompX[{Et}],0.]]; In Omega; Integration Int_1; Jacobian JVol; } } }
      { Name poyfull ; Value{ Local{ [ 0.5*Re[ Cross[({Et}+sEl*I[]*$Eigenvalue* {El}) , Conj[siwt*I[]/(mur[]*mu0*omega0)*norm_dint*(
                                      	{d Et}
                                      	+sEl*I[]*$Eigenvalue* {d El}
                                      	+I[]*sigz*Complex[$EigenvalueReal,$EigenvalueImag]*Vector[-CompY[{Et}],CompX[{Et}],0.]
                                      		      )]]] ] ; In Omega; Integration Int_1; Jacobian JVol; } } }
      { Name vp; 
        Value { Local { [Complex[ ($EigenvalueReal), ($EigenvalueImag)] ]; In Point_guide_middle_bottom ; Jacobian JVol; } } }
    }
  }
} 


PostOperation {  
  { Name SaveEigen_eG; NameOfPostProcessing Guide_e_2D_G;
    Operation {      
      Print[vp[Point_guide_middle_bottom], OnRegion Point_guide_middle_bottom, File StrCat[myDir,  "eigs2D.txt"], Format Table] ;       	  
    }
  }

  { Name SaveField_eG; NameOfPostProcessing Guide_e_2D_G;
    Operation{ 
    	Print[ efull  , OnElementsOf Omega, File StrCat[myDir,  "Emodes.pos"  ] , Format Gmsh, EigenvalueLegend] ;
    	Print[ efull  , OnElementsOf Omega, File StrCat[myDir,  "Emode_interest.pos"  ] , Format Gmsh, TimeStep {1}] ;
    	Print[ efull, OnGrid { $A, $B, 0} 
    	          { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
    		          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
    		          {0.} }, 
    	          File StrCat[myDir, "Emodes_grid.pos"]];
    	Print[ efull, OnGrid { $A, $B, 0} 
    	          { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
    		          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
    		          {0.} }, 
    	          File StrCat[myDir, "Emodes_grid.txt"], Format Table];
    	Print[ efull, OnGrid { $A, $B, 0} 
    	          { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
    		          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
    		          {0.} }, 
    	          File StrCat[myDir, "Emodes_grid.pos"]];
    	Print[ epsrefull, OnGrid { $A, $B, 0} 
    	          { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
    		          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
    		          {0.} }, 
    	          File StrCat[myDir, "epsrEmodes_grid.txt"], Format Table];
      Print[ hfull, OnGrid { $A, $B, 0} 
    	          { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
                  {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
    		          {0.} }, 
    	          File StrCat[myDir, "Hmodes_grid.txt"], Format Table];
      Print[ poyfull, OnGrid { $A, $B, 0}
    	          { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)},
                  {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)},
    		          {0.} },
    		        File StrCat[myDir, "Poymodes_grid.txt"], Format Table];
    	Print[ efull, OnGrid { $A, $B, 0} 
    	          { {x_export2Dm_plot : x_export2Dp_plot+delta_interp : (x_export2Dp_plot-x_export2Dm_plot)/(npts_export2D_x_plot-1)}, 
    		          {y_export2Dm_plot : y_export2Dp_plot+delta_interp : (y_export2Dp_plot-y_export2Dm_plot)/(npts_export2D_y_plot-1)}, 
    		          {0.} }, 
    	          File StrCat[myDir, "Emodes_grid_plot.txt"], Format Table];
    }
  }
}

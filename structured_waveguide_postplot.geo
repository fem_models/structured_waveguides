flag_3D = 1;
flag_sym=1;
Include "structured_waveguide_direct.geo";
Merge "./run_results/dim3D_Poy_tot.pos";
Merge "./run_results/dim3D_normE_tot2_rods.pos";

View[0].Visible = 0;

Plugin(CutGrid).X0 = .1 ;
Plugin(CutGrid).Y0 = -11.65 ;
Plugin(CutGrid).Z0 = 3.85 ;
Plugin(CutGrid).X1 = .1 ;
Plugin(CutGrid).Y1 = -11.65 ;
Plugin(CutGrid).Z1 = 20.45 ;
Plugin(CutGrid).X2 = .1 ;
Plugin(CutGrid).Y2 = 5.85 ;
Plugin(CutGrid).Z2 = 3.85 ;
Plugin(CutGrid).ConnectPoints = 1;
Plugin(CutGrid).NumPointsU = 20 ;
Plugin(CutGrid).NumPointsV = 50 ;
Plugin(CutGrid).View       = 0 ;
Plugin(CutGrid).Run;

Plugin(CutGrid).X0 = 0 ;
Plugin(CutGrid).Y0 = -7 ;
Plugin(CutGrid).Z0 = 3.9 ;
Plugin(CutGrid).X1 = 7 ;
Plugin(CutGrid).Y1 = -7 ;
Plugin(CutGrid).Z1 = 3.9 ;
Plugin(CutGrid).X2 = 0 ;
Plugin(CutGrid).Y2 = 2 ;
Plugin(CutGrid).Z2 = 3.9 ;
Plugin(CutGrid).ConnectPoints = 1;
Plugin(CutGrid).NumPointsU = 50 ;
Plugin(CutGrid).NumPointsV = 60 ;
Plugin(CutGrid).Run;

Plugin(CutGrid).Z0 = 20.2 ;
Plugin(CutGrid).Z1 = 20.2 ;
Plugin(CutGrid).Z2 = 20.2 ;
Plugin(CutGrid).Run;


// View.ShowScale    = 0;
// View.RangeType    = 3;
View[2].RangeType = 2;
View[3].RangeType = 2;
View[4].RangeType = 2;

View[2].ArrowSizeMax = 90;
View[3].ArrowSizeMax = 90;
View[4].ArrowSizeMax = 90;

View[2].CustomMax = 0.0505;
View[3].CustomMax = 0.0505;
View[4].CustomMax = 0.0505;
View[2].CustomMin = 0.;
View[3].CustomMin = 0.;
View[4].CustomMin = 0.;

View[1].ColormapNumber = 7;
View[2].ColormapNumber = 14;
View[3].ColormapNumber = 14;
View[4].ColormapNumber = 14;
View[1].ShowScale = 0;
View[2].ShowScale = 0;
View[3].ShowScale = 0;
View[4].ShowScale = 0;


Geometry.Points = 0;
Geometry.Color.Lines = {0,0,0};
Show "*";
Hide {
Point{318,319,320,321,322,323,324,325,1000,1010,1100,1110,1200,1210,1300,1310};
Curve{826,827,828,829,830,831,832,833,834,835,836};
Surface{838,840,842,844,849,1504};
Volume{1510,1512};
}

// General.BoundingBoxSize = 32.40759324602801;
// General.ClipPositionX = 1046;
// General.ClipPositionY = 189;
// General.FieldHeight = 368;
// General.FieldWidth = 481;
// General.GraphicsFontSize = 30;
// General.GraphicsFontSizeTitle = 28;
// General.GraphicsHeight = 929;
// General.GraphicsPositionX = 4;
// General.GraphicsPositionY = 65;
// General.GraphicsWidth = 1722;
// General.MaxX = 12.39;
// General.MaxY = 5.85;
// General.MaxZ = 24.3;
// General.MenuWidth = 419;
// General.MinY = -11.65;
// General.OptionsPositionX = 4;
// General.OptionsPositionY = 262;
// General.PluginPositionX = 25;
// General.PluginPositionY = 604;
// General.PluginHeight = 368;
// General.PluginWidth = 523;
// General.RotationX = 185.1034707838577;
// General.RotationY = 61.59502481751336;
// General.RotationZ = 180.372395989442;
// General.ScaleX = 1.249640157691438;
// General.ScaleY = 1.249640157691438;
// General.ScaleZ = 1.249640157691438;
// General.SmallAxesPositionX = -90;
// General.SmallAxesPositionY = -320;
// General.SmallAxesSize = 60;
// General.TrackballQuaternion0 = -0.0255838365281872;
// General.TrackballQuaternion1 = -0.8580517742826663;
// General.TrackballQuaternion2 = -0.03990518559229917;
// General.TrackballQuaternion3 = 0.5113708987825869;
// General.TranslationX = 0.03237071049337636;
// General.TranslationY = -0.01879899547902947;
// General.VisibilityPositionX = 120;
// General.VisibilityPositionY = 155;

General.GraphicsFont = "Courier-Bold";
General.GraphicsFontSize = 35;
General.ClipPositionX = 1046;
General.ClipPositionY = 189;
General.FieldHeight = 368;
General.FieldWidth = 481;
General.GraphicsHeight = 929;
General.GraphicsPositionX = 4;
General.GraphicsPositionY = 65;
General.GraphicsWidth = 1722;
General.MaxX = 12.39;
General.MaxY = 5.85;
General.MaxZ = 24.3;
General.MenuWidth = 419;
General.MinY = -11.65;
General.OptionsPositionX = 4;
General.OptionsPositionY = 262;
General.PluginPositionX = 25;
General.PluginPositionY = 604;
General.PluginHeight = 368;
General.PluginWidth = 523;
General.RotationX = 5.496986524533598;
General.RotationY = 65.56909978026043;
General.RotationZ = 0.07401247037000082;
General.ScaleX = 1.249640157691438;
General.ScaleY = 1.249640157691438;
General.ScaleZ = 1.249640157691438;
General.SmallAxesPositionX = -690;
General.SmallAxesPositionY = -735;
General.SmallAxesSize = 70;
General.TrackballQuaternion0 = -0.04066305791539341;
General.TrackballQuaternion1 = -0.5408324822475232;
General.TrackballQuaternion2 = -0.02650741700667776;
General.TrackballQuaternion3 = 0.8397285863364348;
General.TranslationX = 0.02572269063098598;
General.TranslationY = -0.02318010383818359;
General.VisibilityPositionX = 120;
General.VisibilityPositionY = 155;

Print "fig_poynting.png";

// cp fig_poynting.png ~/ownCloud/articles_encours/2019_guide_louise_paper/paper_guide_louise_v2


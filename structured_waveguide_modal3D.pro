Include "parameters_gmsh_getdp.dat";

Group{
	Subs          = Region[1];
	Low           = Region[2];
	Guide         = Region[3];
	Embed         = Region[{}];
	Super         = Region[5];
  Truc          = Region[14];
	
  PMLsubs_x     = Region[6];
	PMLsubs_y     = Region[7];
	PMLsubs_xy    = Region[8];
	PMLlow_x      = Region[9];
	PMLembed_x    = Region[{}];
	PMLsuper_x    = Region[11];
	PMLsuper_y    = Region[12];
	PMLsuper_xy   = Region[13];

  Surf_Bloch_zp = Region[102];
  Surf_Bloch_zm = Region[101];
  Surf_Bloch    = Region[{Surf_Bloch_zm,Surf_Bloch_zp}];
  Surf_Sym_x0   = Region[110];

  Surf_guide    = Region[{}];
  
	Surf_Diri_out = Region[120];
	PrintPoint    = Region[1000];
	
	PML_xy        = Region[{PMLsubs_xy,PMLsuper_xy}];
	PML_x         = Region[{PMLsubs_x,PMLlow_x,PMLembed_x,PMLsuper_x}];
	PML_y         = Region[{PMLsubs_y,PMLsuper_y}];

	PMLs   = Region[{PML_xy,PML_x,PML_y}];
  Domain = Region[{Subs,Low,Guide,Embed,Super,Truc}];
  Omega  = Region[{Domain,PMLs}];
}

Function {
  // norm_dint = 1e-6/um;
  plotlog_offset=1e-7;

  I[]     = Complex[0.,1.];
  EZ[]    = Vector[0.,0.,1.];
  // EZ2[]  = ComplexVectorField[XYZ[]]{1};
  omega0  = 2.*Pi*cel/lambda0;
  k0      = 2.*Pi/lambda0;

  a_pml       =  1.;
  b_pml       = siwt*1.;

  sx[Domain]  =  1.;
  sy[Domain]  =  1.;
  sz[Domain]  =  1.;

  sx[PML_xy]  = Complex[a_pml,-b_pml];
  sy[PML_xy]  = Complex[a_pml,-b_pml];
  sz[PML_xy]  = 1.;
  sx[PML_x]   = Complex[a_pml,-b_pml];
  sy[PML_x]   = 1.;
  sz[PML_x]   = 1.;
  sx[PML_y]   = 1.;
  sy[PML_y]   = Complex[a_pml,-b_pml];
  sz[PML_y]   = 1.;
  Lxx[]       = sy[]*sz[]/sx[];
  Lyy[]       = sz[]*sx[]/sy[]; 
  Lzz[]       = sx[]*sy[]/sz[];
  PML_tens[]  = TensorDiag[Lxx[],Lyy[],Lzz[]];

  eps_subs[]  = Complex[eps_subs,0.];
  eps_low[]   = Complex[eps_low,0.];
  eps_guide[] = Complex[eps_guide,0.];
  eps_embed[] = 0;
  eps_super[] = Complex[eps_super,0.];
  eps_truc[]  = Complex[eps_truc_re,eps_truc_im];

  epsilonr[Subs]         = eps_subs[]  * TensorDiag[1.,1.,1.];
  epsilonr[Low]          = eps_low[]   * TensorDiag[1.,1.,1.];
  epsilonr[Guide]        = eps_guide[] * TensorDiag[1.,1.,1.];
  epsilonr[Embed]        = eps_embed[] * TensorDiag[1.,1.,1.];
  epsilonr[Super]        = eps_super[] * TensorDiag[1.,1.,1.];
  epsilonr[Truc]         = eps_truc[]  * TensorDiag[1.,1.,1.];
  epsilonr[PMLsubs_x]    = eps_subs[]  * PML_tens[];
  epsilonr[PMLsubs_y]    = eps_subs[]  * PML_tens[];
  epsilonr[PMLsubs_xy]   = eps_subs[]  * PML_tens[];
  epsilonr[PMLlow_x]     = eps_low[]   * PML_tens[];
  epsilonr[PMLembed_x]   = 0;
  epsilonr[PMLsuper_x]   = eps_super[] * PML_tens[];
  epsilonr[PMLsuper_y]   = eps_super[] * PML_tens[];
  epsilonr[PMLsuper_xy]  = eps_super[] * PML_tens[];

  mur[Subs]        = TensorDiag[1.,1.,1.];
  mur[Low]         = TensorDiag[1.,1.,1.];
  mur[Guide]       = TensorDiag[1.,1.,1.];
  mur[Embed]       = TensorDiag[1.,1.,1.];
  mur[Super]       = TensorDiag[1.,1.,1.];
  mur[Truc]        = TensorDiag[1.,1.,1.];
  mur[PMLsubs_x]   = PML_tens[];
  mur[PMLsubs_y]   = PML_tens[];
  mur[PMLsubs_xy]  = PML_tens[];
  mur[PMLlow_x]    = PML_tens[];
  mur[PMLembed_x]  = PML_tens[];
  mur[PMLsuper_x]  = PML_tens[];
  mur[PMLsuper_y]  = PML_tens[];
  mur[PMLsuper_xy] = PML_tens[];
}

Constraint {
  { Name Dirichlet_PEC; Type Assign;
    Case {
      { Region Region[Surf_Diri_out]; Value 0.; }
    }
  }
  { Name Dirichlet_sym; Type Assign;
    Case {
      { Region Region[Surf_Sym_x0]; Value 0.; }
    }
  }
	{Name Bloch;
		Case {
			{ Region Surf_Bloch_zp; Type LinkCplx ; RegionRef Surf_Bloch_zm ;
				Coefficient Complex[1.,0.]; Function Vector[$X,$Y,$Z-d_truc] ;
			}
		}
	}
}


Integration {
  { Name CurlCurl ;
    Case { 
      { Type Gauss ;
        Case {  
          { GeoElement Point       ; NumberOfPoints  1 ; }
          { GeoElement Line        ; NumberOfPoints  3 ; }
          { GeoElement Triangle    ; NumberOfPoints  4 ; }
          { GeoElement Tetrahedron ; NumberOfPoints  15 ; }
				}
      }
    }
  }
}


Jacobian {
  { Name Vol ;
    Case{
      { Region All ; Jacobian Vol ; }
    }
  }
  { Name Sur ;
    Case{
      { Region All ; Jacobian Sur ; }
    }
  }
  { Name Lin ;
    Case{
      { Region All ; Jacobian Lin ; }
    }
  }
}

FunctionSpace {
	{ Name H0curl; Type Form1;
	  BasisFunction {
      { Name sn;  NameOfCoef un ; Function BF_Edge      ; Support Region[Omega]; Entity EdgesOf[All]; }
      { Name sn2; NameOfCoef un2; Function BF_Edge_2E   ; Support Region[Omega]; Entity EdgesOf[All]; }
        If(flag_o2==1)
          { Name sn3; NameOfCoef un3; Function BF_Edge_3F_b ; Support Region[Omega]; Entity FacetsOf[Omega, Not Surf_Bloch]; }
          { Name sn4; NameOfCoef un4; Function BF_Edge_3F_c ; Support Region[Omega]; Entity FacetsOf[Omega, Not Surf_Bloch]; }
          { Name sn5; NameOfCoef un5; Function BF_Edge_4E   ; Support Region[Omega]; Entity EdgesOf[Omega, Not Surf_Bloch]; }
        EndIf
     }
	 Constraint {
		If(flag_sym_diri==1)
		   { NameOfCoef un;  EntityType EdgesOf  ; NameOfConstraint Dirichlet_sym; }
		   { NameOfCoef un2; EntityType EdgesOf  ; NameOfConstraint Dirichlet_sym; }
       If(flag_o2==1)
         { NameOfCoef un3; EntityType FacetsOf ; NameOfConstraint Dirichlet_sym; }
         { NameOfCoef un4; EntityType FacetsOf ; NameOfConstraint Dirichlet_sym; }
         { NameOfCoef un5; EntityType EdgesOf  ; NameOfConstraint Dirichlet_sym; }
       EndIf
		 EndIf
     { NameOfCoef un;  EntityType EdgesOf  ; NameOfConstraint Bloch; }
     { NameOfCoef un2; EntityType EdgesOf  ; NameOfConstraint Bloch; }
     // { NameOfCoef un;  EntityType EdgesOf  ; NameOfConstraint Dirichlet_PEC; }
     // { NameOfCoef un2; EntityType EdgesOf  ; NameOfConstraint Dirichlet_PEC; }
     If(flag_o2==1)
       { NameOfCoef un3; EntityType FacetsOf ; NameOfConstraint Dirichlet_PEC; }
       { NameOfCoef un4; EntityType FacetsOf ; NameOfConstraint Dirichlet_PEC; }
       { NameOfCoef un5; EntityType EdgesOf  ; NameOfConstraint Dirichlet_PEC; }
     // { NameOfCoef un3; EntityType FacetsOf ; NameOfConstraint Bloch; }
     // { NameOfCoef un4; EntityType FacetsOf ; NameOfConstraint Bloch; }
     // { NameOfCoef un5; EntityType EdgesOf  ; NameOfConstraint Bloch; }
     EndIf
	 }
	}
  
  { Name Hgrad; Type Form0;
    BasisFunction {
      { Name sn;  NameOfCoef un;  Function BF_Node;    Support Region[Omega]; Entity NodesOf[Omega]; }
      { Name sn2; NameOfCoef un2; Function BF_Node_2E; Support Region[Omega]; Entity EdgesOf[Omega]; }
      If(flag_o2==1)
        { Name sn3; NameOfCoef un3; Function BF_Node_3E; Support Region[Omega]; Entity EdgesOf[Omega, Not Surf_Bloch]; }
        { Name sn4; NameOfCoef un4; Function BF_Node_3F; Support Region[Omega]; Entity FacetsOf[Omega, Not Surf_Bloch]; }
      EndIf
    }
    Constraint {
        { NameOfCoef un;  EntityType NodesOf ; NameOfConstraint Bloch; }
        { NameOfCoef un2; EntityType EdgesOf ; NameOfConstraint Bloch; }
        If(flag_sym_diri==1)
          { NameOfCoef un;  EntityType NodesOf  ; NameOfConstraint Dirichlet_sym; }
          { NameOfCoef un2; EntityType EdgesOf  ; NameOfConstraint Dirichlet_sym; }
          If(flag_o2==1)
            { NameOfCoef un3; EntityType FacetsOf ; NameOfConstraint Dirichlet_sym; }
            { NameOfCoef un4; EntityType VolumesOf ; NameOfConstraint Dirichlet_sym; }
            { NameOfCoef un5; EntityType EdgesOf  ; NameOfConstraint Dirichlet_sym; }
          EndIf
        EndIf
        // // PEC PML
        // { NameOfCoef un;  EntityType NodesOf  ; NameOfConstraint Dirichlet_PEC; }
        // { NameOfCoef un2; EntityType EdgesOf  ; NameOfConstraint Dirichlet_PEC; }
        // If(flag_o2==1)
        //   { NameOfCoef un3; EntityType FacetsOf ; NameOfConstraint Dirichlet_PEC; }
        //   { NameOfCoef un4; EntityType VolumesOf ; NameOfConstraint Dirichlet_PEC; }
        //   { NameOfCoef un5; EntityType EdgesOf  ; NameOfConstraint Dirichlet_PEC; }
        // EndIf
    }
  }
}

Formulation {
	{Name modal_3D_k; Type FemEquation;
		Quantity {
			{ Name u     ; Type Local; NameOfSpace H0curl;}
      { Name phi   ; Type Local; NameOfSpace Hgrad ;}
      // { Name weight; Type Local; NameOfSpace H ;}
		}
		Equation {
      // // E-Formulation
      Galerkin {        [ (-1)*     1/mur[]       * Dof{Curl u}  ,  {Curl u} ]; In Omega; Jacobian Vol; Integration CurlCurl;}
      Galerkin {        [ (+1)* epsilonr[]*k0^2   * Dof{     u}  ,  {     u} ]; In Omega; Jacobian Vol; Integration CurlCurl;}
      Galerkin {DtDof   [ (-1)*     EZ[]/\(1/mur[]* Dof{Curl u}) ,  {     u} ]; In Omega; Jacobian Vol; Integration CurlCurl;}
      Galerkin {DtDof   [ (-1)*      1/mur[]*(EZ[]/\Dof{     u}) ,  {Curl u} ]; In Omega; Jacobian Vol; Integration CurlCurl;}
      Galerkin {DtDtDof [ ( 1)*      1/mur[]*(EZ[]/\Dof{     u}) , EZ[]/\{u} ]; In Omega; Jacobian Vol; Integration CurlCurl;}
      
      Galerkin {        [ (+1)* epsilonr[]*       Dof{Grad phi}  ,       {       u}  ]; In Omega; Jacobian Vol; Integration CurlCurl;}
      Galerkin {DtDof   [ (+1)* epsilonr[]* EZ[]*Dof{     phi}   ,       {       u}  ]; In Omega; Jacobian Vol; Integration CurlCurl;}

      Galerkin {        [ (+1)* Conj[     epsilonr[]*Dof{       u}] ,  Conj[{Grad phi}] ]; In Omega; Jacobian Vol; Integration CurlCurl;}
      Galerkin {DtDof   [ (-1)* Conj[EZ[]*epsilonr[]*Dof{       u}] ,  Conj[{     phi}] ]; In Omega; Jacobian Vol; Integration CurlCurl;}
		}
	}
}

Resolution {  
  { Name res_modal_3D_k;
    System {
			{ Name A; NameOfFormulation modal_3D_k; Type ComplexValue; }
	 } 
    Operation {  
      // GmshRead[StrCat[myDir, "v.pos"],1];
			GenerateSeparate[A]; 
			EigenSolve[A,neig,shift_quad_re,shift_quad_im]; 
    } 
  } 
}

PostProcessing {
  { Name postpro_modal_3D_k; NameOfFormulation modal_3D_k;
    Quantity {
			{ Name phi         ; Value { Local { [ {phi}     ]; In Omega; Jacobian Vol; } } }
			{ Name epsilonr    ; Value { Local { [ epsilonr[]]; In Omega; Jacobian Vol; } } }
			{ Name Emodes_per  ; Value { Local { [ {u}       ]; In Omega; Jacobian Vol; } } }
			{ Name Emodes_norm ; Value { Local { [ Norm[{u}] ]; In Omega; Jacobian Vol; } } }
			{ Name Emodes      ; Value { Local { [ {u}*Complex[Cos[$EigenvalueReal*Z[]],Sin[$EigenvalueReal*Z[]]] * Exp[-$EigenvalueImag*Z[]]  ]; In Omega; Jacobian Vol; } } }
			{ Name eigs        ; Value { Local { [ Complex[$EigenvalueReal,$EigenvalueImag]]; In PrintPoint; Jacobian Vol; } } }
    }
  }
}

PostOperation {
  { Name postop_modal_3D_k; NameOfPostProcessing postpro_modal_3D_k;
    Operation {      
      Print[ Emodes_norm    , OnElementsOf Omega  , File StrCat[myDir, "Emodes_norm.pos"] , EigenvalueLegend ];
      Print[ Emodes         , OnElementsOf Omega  , File StrCat[myDir, "Emodes.pos"]      , EigenvalueLegend ];
      Print[ phi            , OnElementsOf Omega  , File StrCat[myDir, "phi.pos"]         , EigenvalueLegend ];
      Print[eigs[PrintPoint], OnRegion PrintPoint , File StrCat[myDir,  "eigs.txt"]       ,      Format Table];
    }
  }
}
"""
@author: Guillaume Demesy
Direct problem 3D louise stack with above N ellipsoidal rods 'trucs':
"""

import numpy as np
import scipy as sc
import matplotlib
matplotlib.use('Agg')
import pylab as pl
import os
import sys
from structured_waveguide_utils import *

pi=np.pi
np.set_printoptions(precision=3)

flag_run_server = False

str_gmsh_path   = '~/programs/gmsh-4.4.1-Linux64/bin/'
str_getdp_path  = '~/programs/getdp/build/'

flag_light_out    = False
flag_opengmsh     = True

paramaille2D      = 10
paramaille3D      = 2.5
paramaille3Dmodal = 4

nb_hs  = 2
nb_rls = 2
nb_rts = 2

str_2Dmesh_filename= 'invariant_waveguide2D.msh'
str_3Dmesh_filename= 'structured_waveguide3D.msh'

str_pro2D_filename = 'invariant_waveguide_modal2D.pro'
str_pro3D_filename = 'structured_waveguide_direct3D.pro'
myDir              = 'run_results/'

fname_param_gmsh_getdp = 'parameters_gmsh_getdp.dat'

um         =  1.
norm_dint  = 1.
siwt       = -1.
sigz       =  1.

flag_pbtype = sys.argv[1]

if sys.argv[2]=='sym':
    flag_sym = True
elif sys.argv[2]=='full':
    flag_sym = False
else:
    print('Command Line argument error, aborting...');sys.exit()
if not os.path.exists(myDir):
    os.makedirs(myDir)

target_re      =  2.05
target_im      = -2e-2
shift_lin  = (target_re+1j*target_im)**2
shift_quad = (target_re+1j*target_im)*1j

neig          = 6

## symmetry
flag_sym_diri = True
# flag_sym_diri = False

## visualize the 2D modes and select appropriate 
which_mode_nb = 0

epsilon0     = 8.854187817620389e-6*um
mu0          = .4*pi*um
cel          = 1/(np.sqrt(epsilon0 * mu0))

lambda0      = 7.7*um
omega0       = 2.*pi*cel/lambda0

pml_size     = 0.5*lambda0
h_above      = 1*um
space_pml_x  = lambda0/5
h_subs       = .3*um

h_low        = 5.3*um
h_guide      = 2.2*um #3
w_guide      = 14.*um   # largeur totale, on maille 1/2

N_truc  = 4
d_truc  = 4
h_truc  = 1
rl_truc = 1.5
rt_truc = 5

w_box_trucs  = rt_truc+0.25*um
h_box_trucs  = h_truc+0.25*um
h_pmllat     = pml_size
h_pmltop     = pml_size
h_pmlbot     = pml_size
h_pmlzm      = pml_size
h_pmlzp      = pml_size

space_pml_zm = 0.3*um #lambda0/4.
space_pml_zp = 0.3*um #lambda0/4.

eps_subs     = 11.69024481 # 1.  # silicium n=3.4191 at 7.7microns
eps_low      = 2.49**2 # before 17/03/17 : 2.42**2 # se2 #5.8081      # 1.  # Se2 n=2.41 at 7.7 microns
eps_super    = 1.
eps_guide    = 2.68**2 # before 17/03/17 2.62**2 #se4 #7.8961   # Se6 n=2.81 at 7.7 microns
eps_truc     = 9+1j*2  # Olmon et al. 2012: Evaporated gold
eps_box      = 1+0.*1j

y_export2Dp_plot = h_above+h_truc
y_export2Dm_plot = -h_subs-h_guide-h_low
x_export2Dp_plot = space_pml_x+w_guide/2.0
x_export2Dm_plot = -x_export2Dp_plot
delta_interp = 0.00001*um
npts_export2D_x_plot = 50
npts_export2D_y_plot = 50
delta_probe     = 0.001*um

## export 2D fields param
y_export2Dp = h_above+h_truc+h_pmltop
y_export2Dm = -h_subs-h_guide-h_low-h_pmlbot
x_export2Dp = space_pml_x+w_guide/2.0+h_pmllat
x_export2Dm = 0.
delta_interp = 0.00001*um
npts_export2D_x = 60
npts_export2D_y = 120
delta_probe     = 0.001*um

## export 3D out poynting
dist2trucpml_z      = space_pml_zm/1.2
dist2guide_xy       = lambda0/20.
x_export_poy3Dp     = w_guide/2.0+space_pml_x+h_pmllat
x_export_poy3Dm     = 0.
y_export_poy3Dp     = h_truc+h_above+h_pmltop
y_export_poy3Dm     = -h_guide-h_low-h_subs-h_pmlbot
dxy                 = 0.2*um
npts_export_poy3D_z = 1
npts_export_poy3D_x = int((x_export_poy3Dp-x_export_poy3Dm)/dxy)
npts_export_poy3D_y = int((y_export_poy3Dp-y_export_poy3Dm)/dxy)
x_export3Dm = 0.
x_export3Dp = (w_guide/2.)*0.2
z_export3Dm = h_pmlzm
dxz = 10. * 1e-3*um
npts_export3D_x = int((x_export3Dp-x_export3Dm)/dxz)
dist2truc_cuty = 0.02*um

z_export3Dp         = h_pmlzm+space_pml_zp+space_pml_zm+N_truc*d_truc
z_export_poy3Dp_min = h_pmlzm+space_pml_zm+N_truc*d_truc+dist2trucpml_z
z_export_poy3Dp_max = h_pmlzm+space_pml_zm+N_truc*d_truc+space_pml_zp-dist2trucpml_z
z_export_poy3Dm_min = h_pmlzm+dist2trucpml_z
z_export_poy3Dm_max = h_pmlzm+space_pml_zm-dist2trucpml_z
npts_export3D_z     = int((z_export3Dp-z_export3Dm)/dxz)

# TODO deal with flag_o2
# TODO sym 3D modal diri
# TODO order 2 3D modal diri

flag_o2 = False

# share global variables with gmsh/getdp
DumpAllToTxt(globals(),fname_param_gmsh_getdp)

if flag_pbtype == 'direct':
    ## compute modes of the invariant structure
    flag_3D = False
    os.system(str_gmsh_path+'gmsh -setnumber flag_3D %g -setnumber flag_sym %g structured_waveguide_direct.geo -2 -o '%(int(flag_3D),int(flag_sym))+str_2Dmesh_filename)
    str_getdp_eigsolve_slepc  = str_getdp_path+'getdp '+str_pro2D_filename+' -pre  Guide_e_2D_G_PVP -msh  '+str_2Dmesh_filename+' -cal -petsc_prealloc 50 -ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps'
    os.system(str_getdp_eigsolve_slepc)
    os.system('cp parameters_gmsh_getdp.dat parameters_gmsh_getdp_2D.dat')
    if flag_opengmsh==True:
        os.system(str_gmsh_path+'gmsh -setnumber flag_3D %g -setnumber flag_sym %g structured_waveguide_direct.geo '%(int(flag_3D),int(flag_sym))+\
        ' '+myDir+'Emodes.pos'\
        ' '+myDir+'Hmodes.pos'\
        ' '+myDir+'Poymodes.pos')

    # load 2D modes and set them as incident 3D fields
    flag_3D = True
    res2D=get_all_eigenvectors_ongrid(myDir+'Emodes_grid.txt',npts_export2D_x,npts_export2D_y,neig)
    tab_x=res2D['tab_x']
    tab_y=res2D['tab_y']
    eigenfieldsE=res2D['fields']
    eigenfieldsH=get_all_eigenvectors_ongrid(myDir+'Hmodes_grid.txt',npts_export2D_x,npts_export2D_y,neig)['fields']
    # epsreigenfieldsE=get_all_eigenvectors_ongrid(myDir+'epsrEmodes_grid.txt',npts_export2D_x,npts_export2D_y,neig)['fields']
    convert_getdp_IO_complexvector(myDir+'Emodes_grid.txt',myDir+'Emode_grid_feed3D',r"E_",npts_export2D_x,npts_export2D_y,which_mode_nb,False)
    convert_getdp_IO_complexvector(myDir+'Hmodes_grid.txt',myDir+'Hmode_grid_feed3D',r"H_",npts_export2D_x,npts_export2D_y,which_mode_nb,False)
    convert_getdp_IO_complexvector(myDir+'Poymodes_grid.txt',myDir+'Poymode_grid_feed3D',r"S_",npts_export2D_x,npts_export2D_y,which_mode_nb,False)
    
    # load 2D eigenvalues gamma
    gamma_mode_re = np.loadtxt(myDir+'eigs2D.txt')[which_mode_nb,1]
    gamma_mode_im = np.loadtxt(myDir+'eigs2D.txt')[which_mode_nb,2]
    gamma_mode = gamma_mode_re+1j*gamma_mode_im
    gammas     = np.loadtxt(myDir+'eigs2D.txt')[:,1]+1j*np.loadtxt(myDir+'eigs2D.txt')[:,2]
    UpdateParam('paramaille',paramaille3D,fname_param_gmsh_getdp)
    UpdateParam('gamma_mode',gamma_mode,fname_param_gmsh_getdp)

    # init
    Poynting_mode_in           = np.ones((npts_export_poy3D_z),dtype=complex)
    Poynting_mode_out          = np.ones((npts_export_poy3D_z),dtype=complex)
    Poynting_tot_out_zm        = np.ones((npts_export_poy3D_z),dtype=complex)
    Poynting_tot_out_zp        = np.ones((npts_export_poy3D_z),dtype=complex)
    Poynting_dif_out_zm        = np.ones((npts_export_poy3D_z),dtype=complex)
    Poynting_dif_out_zp        = np.ones((npts_export_poy3D_z),dtype=complex)
    an_ti                      = np.ones((neig),dtype=complex)
    an_t                       = np.ones((neig),dtype=complex)
    an_r                       = np.ones((neig),dtype=complex)

    # run 3D
    os.system(str_gmsh_path+'gmsh -setnumber flag_3D %g -setnumber flag_sym 1 structured_waveguide_direct.geo -3 -o '%(int(flag_3D))+str_3Dmesh_filename)
    os.system(str_getdp_path+'getdp '+str_pro3D_filename+' -pre  res_helmholtz3D -msh  '+str_3Dmesh_filename+' -cal -petsc_prealloc 500 -ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps -pos  postop_helmholtz3D -bin')
    if flag_opengmsh==True:
        os.system(str_gmsh_path+'gmsh -setnumber flag_3D %g -setnumber flag_sym %g structured_waveguide_postplot.geo '%(int(flag_3D),int(flag_sym)))
    
    # compute energy balance
    for k in range(npts_export_poy3D_z):
        Poynting_mode_in[k]    = dtrap_poy(myDir+'cut_Poymode3D_grid_zm_%g.txt'%(k),npts_export_poy3D_x,npts_export_poy3D_y)
        Poynting_tot_out_zm[k] = dtrap_poy(myDir+'cut_Poy_tot_grid_zm_%g.txt'%(k),npts_export_poy3D_x,npts_export_poy3D_y)
        Poynting_tot_out_zp[k] = dtrap_poy(myDir+'cut_Poy_tot_grid_zp_%g.txt'%(k),npts_export_poy3D_x,npts_export_poy3D_y)
        Poynting_dif_out_zm[k] = dtrap_poy(myDir+'cut_Poy_dif_grid_zm_%g.txt'%(k),npts_export_poy3D_x,npts_export_poy3D_y)
        Poynting_dif_out_zp[k] = dtrap_poy(myDir+'cut_Poy_dif_grid_zp_%g.txt'%(k),npts_export_poy3D_x,npts_export_poy3D_y)
    int_normE2_tot = np.loadtxt(myDir+'int_normE2_tot.txt')[1]
    T_in_guide = np.mean(Poynting_tot_out_zp,axis=-1)/np.mean(Poynting_mode_in,axis=-1)
    R_in_guide = np.mean(Poynting_dif_out_zm,axis=-1)/np.mean(Poynting_mode_in,axis=-1)
    A_in_guide = int_normE2_tot/np.mean(Poynting_mode_in,axis=-1)
    Tot        = T_in_guide-R_in_guide+A_in_guide
        
    # expand the scattered/total field on modes of the invariant structure
    E_dif_2expand     = get_all_eigenvectors_ongrid(myDir+'E_dif_2expand.txt',npts_export2D_x,npts_export2D_y,1)['fields']
    E_tot_2expand     = get_all_eigenvectors_ongrid(myDir+'E_tot_2expand.txt',npts_export2D_x,npts_export2D_y,1)['fields']
    E_inc_2expand     = get_all_eigenvectors_ongrid(myDir+'E_inc_2expand.txt',npts_export2D_x,npts_export2D_y,1)['fields']
    deph   = np.exp(-1j*gammas*z_export_poy3Dp_max)
    for n in range(neig):
        integrand_r   = -E_dif_2expand[0,0,:,:]*eigenfieldsH[n,1,:,:]+E_dif_2expand[0,1,:,:]*eigenfieldsH[n,0,:,:]
        integrand_t   = -E_tot_2expand[0,0,:,:]*eigenfieldsH[n,1,:,:]+E_tot_2expand[0,1,:,:]*eigenfieldsH[n,0,:,:]
        integrand_ti  = -E_inc_2expand[0,0,:,:]*eigenfieldsH[n,1,:,:]+E_inc_2expand[0,1,:,:]*eigenfieldsH[n,0,:,:]
        integrand_den =  -eigenfieldsE[n,0,:,:]*eigenfieldsH[n,1,:,:]+ eigenfieldsE[n,1,:,:]*eigenfieldsH[n,0,:,:]
        an_den = np.trapz(np.trapz(integrand_den,tab_x,axis=0),tab_y)
        an_r[n]   = np.trapz(np.trapz(integrand_r,tab_x,axis=0),tab_y)/an_den
        an_t[n]   = np.trapz(np.trapz(integrand_t,tab_x,axis=0),tab_y)/an_den
        an_ti[n]  = np.trapz(np.trapz(integrand_ti,tab_x,axis=0),tab_y)/an_den

    print( 'T_in_guide = ',T_in_guide.real)
    print( 'R_in_guide = ',R_in_guide.real)
    print( 'A_in_guide = ',A_in_guide.real)
    print( 'Tot        = ',Tot.real)
    print( 'sun r_n    = ',np.sum(np.abs(an_r)**2))
    print( 'sun t_n    = ',np.sum(np.abs(an_t)**2))
    
    # check numerical bi-orthogonality
    matbio = np.zeros((neig,neig))
    for ni in range(neig):
        for nj in range(neig):
            integrand_den = -eigenfieldsE[ni,0,:,:]*eigenfieldsH[nj,1,:,:] \
                            +eigenfieldsE[ni,1,:,:]*eigenfieldsH[nj,0,:,:]
            matbio[ni,nj] = np.abs(np.trapz(np.trapz(integrand_den,tab_x,axis=0),tab_y))
    print('bi-orthogonality matrix:\n',matbio)
    saveall(globals(),'energy_results.npz',light=True)

# 3D modal problem    
if flag_pbtype=='modal':
    UpdateParam('flag_3D',True,fname_param_gmsh_getdp)
    flag_sym=False
    str_mesh_filename = 'structured_waveguide_modal3D.msh'
    str_pro_filename  = 'structured_waveguide_modal3D.pro'
    str_geo_filename  = 'structured_waveguide_modal3D.geo'
    # os.system(str_gmsh_path+'gmsh '+str_geo_filename+' -setnumber flag_3D 1 -setnumber flag_sym 0')
    os.system(str_gmsh_path+'gmsh '+str_geo_filename+' -setnumber flag_3D 1 -setnumber flag_sym 0 -3 -o '+str_mesh_filename)
    str_getdp_eigsolve_slepc  = str_getdp_path+'getdp '+str_pro_filename+' -pre  res_modal_3D_k -msh  '+str_mesh_filename+' -cal -petsc_prealloc 500 -pos postop_modal_3D_k -bin -slepc -pep_view'
    os.system(str_getdp_eigsolve_slepc)
    eigs3D = np.loadtxt(myDir+'eigs.txt',usecols=1)+np.loadtxt(myDir+'eigs.txt',usecols=2)*1j
    pl.figure();pl.plot(eigs3D.real,eigs3D.imag,'r+');pl.savefig('eigenvalues_modal3D.pdf')
    if flag_opengmsh==True:
        os.system(str_gmsh_path+'gmsh '+str_geo_filename+' -setnumber flag_3D 1 -setnumber flag_sym 0 '+myDir+'/Emodes.pos')

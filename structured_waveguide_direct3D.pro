Include "parameters_gmsh_getdp.dat";

Group {
	Subs         = Region[1];
	Low          = Region[2];
	Guide        = Region[3];
	Super        = Region[4];
	Truc         = Region[5];
	PMLsubs_x    = Region[6];
	PMLsubs_y    = Region[7];
	PMLsubs_z    = Region[8];
	PMLsubs_xy   = Region[9];
	PMLsubs_yz   = Region[10];
	PMLsubs_zx   = Region[11];
	PMLsubs_xyz  = Region[12];
	PMLlow_x     = Region[13];
	PMLlow_z     = Region[14];
	PMLlow_zx    = Region[15];
	PMLguide_z   = Region[16];
	PMLsuper_x   = Region[17];
	PMLsuper_y   = Region[18];
	PMLsuper_z   = Region[19];
	PMLsuper_xy  = Region[20];
	PMLsuper_yz  = Region[21];
	PMLsuper_zx  = Region[22];
	PMLsuper_xyz = Region[23];
	Surf_Diri    = Region[24];
	Box          = Region[25];
	Surf_Inj     = Region[26];
	
	PML_xyz       = Region[{PMLsuper_xyz,PMLsubs_xyz}];
	PML_zx        = Region[{PMLsuper_zx,PMLsubs_zx,PMLlow_zx}];
	PML_yz        = Region[{PMLsubs_yz,PMLsuper_yz}];
	PML_xy        = Region[{PMLsubs_xy,PMLsuper_xy}];
	PML_x         = Region[{PMLsubs_x,PMLlow_x,PMLsuper_x}];
	PML_y         = Region[{PMLsubs_y,PMLsuper_y}];
	PML_z         = Region[{PMLsubs_z,PMLlow_z,PMLsuper_z,PMLguide_z}];

	PMLs   = Region[{PML_xyz,PML_xy,PML_zx,PML_yz,PML_x,PML_y,PML_z}];
  Domain = Region[{Subs,Low,Guide,Super,Truc,Box}];
  Omega  = Region[{Domain,PMLs}];
}

Function {
  // norm_dint = 1e-6/um;
  plotlog_offset=1e-7;

  I[]     = Complex[0,1];
  Freq    = cel/lambda0;
  omega0  = 2.0*Pi*Freq;
  k0      = 2.0*Pi/lambda0;

  a_pml       =  1.;
  b_pml       = siwt*1.;

  sx[Domain]  =  1.;
  sy[Domain]  =  1.;
  sz[Domain]  =  1.;

  sx[PML_xyz] = Complex[a_pml,-b_pml];
  sy[PML_xyz] = Complex[a_pml,-b_pml];
  sz[PML_xyz] = Complex[a_pml,-b_pml];
  sx[PML_zx]  = Complex[a_pml,-b_pml];
  sy[PML_zx]  = 1.0;
  sz[PML_zx]  = Complex[a_pml,-b_pml];
  sx[PML_yz]  = 1.0;
  sy[PML_yz]  = Complex[a_pml,-b_pml];
  sz[PML_yz]  = Complex[a_pml,-b_pml];
  sx[PML_xy]  = Complex[a_pml,-b_pml];
  sy[PML_xy]  = Complex[a_pml,-b_pml];
  sz[PML_xy]  = 1.0;
  sx[PML_x]   = Complex[a_pml,-b_pml];
  sy[PML_x]   = 1.0;
  sz[PML_x]   = 1.0;
  sx[PML_y]   = 1.0;
  sy[PML_y]   = Complex[a_pml,-b_pml];
  sz[PML_y]   = 1.0;
  sx[PML_z]   = 1.0;
  sy[PML_z]   = 1.0;
  sz[PML_z]   = Complex[a_pml,-b_pml];
  Lxx[]       = sy[]*sz[]/sx[];
  Lyy[]       = sz[]*sx[]/sy[]; 
  Lzz[]       = sx[]*sy[]/sz[];
  PML_tens[]  = TensorDiag[Lxx[],Lyy[],Lzz[]];

  eps_subs[] = Complex[eps_subs,0];
  eps_low[]  = Complex[eps_low,0];
  eps_guide[]= Complex[eps_guide,0];
  eps_super[]= Complex[eps_super,0];
  eps_truc[] = Complex[eps_truc_re,eps_truc_im];
  eps_box[] = Complex[eps_box_re,eps_box_im];

  epsilonr[PMLsubs_x]    = eps_subs[]  * PML_tens[];
  epsilonr[PMLsubs_y]    = eps_subs[]  * PML_tens[];
  epsilonr[PMLsubs_z]    = eps_subs[]  * PML_tens[];
  epsilonr[PMLsubs_xy]   = eps_subs[]  * PML_tens[];
  epsilonr[PMLsubs_yz]   = eps_subs[]  * PML_tens[];
  epsilonr[PMLsubs_zx]   = eps_subs[]  * PML_tens[];
  epsilonr[PMLsubs_xyz]  = eps_subs[]  * PML_tens[];
  epsilonr[PMLlow_x]     = eps_low[]   * PML_tens[];
  epsilonr[PMLlow_z]     = eps_low[]   * PML_tens[];
  epsilonr[PMLlow_zx]    = eps_low[]   * PML_tens[];
  epsilonr[PMLguide_z]   = eps_guide[] * PML_tens[];
  epsilonr[PMLsuper_x]   = eps_super[] * PML_tens[];
  epsilonr[PMLsuper_y]   = eps_super[] * PML_tens[];
  epsilonr[PMLsuper_z]   = eps_super[] * PML_tens[];
  epsilonr[PMLsuper_xy]  = eps_super[] * PML_tens[];
  epsilonr[PMLsuper_zx]  = eps_super[] * PML_tens[];
  epsilonr[PMLsuper_yz]  = eps_super[] * PML_tens[];
  epsilonr[PMLsuper_xyz] = eps_super[] * PML_tens[];
  epsilonr[Subs]         = eps_subs[]  * TensorDiag[1,1,1];
  epsilonr[Low]          = eps_low[]   * TensorDiag[1,1,1];
  epsilonr[Guide]        = eps_guide[] * TensorDiag[1,1,1];
  epsilonr[Super]        = eps_super[] * TensorDiag[1,1,1];
  epsilonr[Truc]         = eps_truc[]  * TensorDiag[1,1,1];
  epsilonr[Box]          = eps_box[]  * TensorDiag[1,1,1];

  mur[PMLsubs_x]    = PML_tens[];
  mur[PMLsubs_y]    = PML_tens[];
  mur[PMLsubs_z]    = PML_tens[];
  mur[PMLsubs_xy]   = PML_tens[];
  mur[PMLsubs_yz]   = PML_tens[];
  mur[PMLsubs_zx]   = PML_tens[];
  mur[PMLsubs_xyz]  = PML_tens[];
  mur[PMLlow_x]     = PML_tens[];
  mur[PMLlow_z]     = PML_tens[];
  mur[PMLlow_zx]    = PML_tens[];
  mur[PMLguide_z]   = PML_tens[];
  mur[PMLsuper_x]   = PML_tens[];
  mur[PMLsuper_y]   = PML_tens[];
  mur[PMLsuper_z]   = PML_tens[];
  mur[PMLsuper_xy]  = PML_tens[];
  mur[PMLsuper_zx]  = PML_tens[];
  mur[PMLsuper_yz]  = PML_tens[];
  mur[PMLsuper_xyz] = PML_tens[];
  mur[Subs]         = TensorDiag[1,1,1];
  mur[Low]          = TensorDiag[1,1,1];
  mur[Guide]        = TensorDiag[1,1,1];
  mur[Super]        = TensorDiag[1,1,1];
  mur[Truc]         = TensorDiag[1,1,1];
  mur[Box]          = TensorDiag[1,1,1];

  epsilonr_annex[PMLsubs_x]    = eps_subs[]  * PML_tens[];
  epsilonr_annex[PMLsubs_y]    = eps_subs[]  * PML_tens[];
  epsilonr_annex[PMLsubs_z]    = eps_subs[]  * PML_tens[];
  epsilonr_annex[PMLsubs_xy]   = eps_subs[]  * PML_tens[];
  epsilonr_annex[PMLsubs_yz]   = eps_subs[]  * PML_tens[];
  epsilonr_annex[PMLsubs_zx]   = eps_subs[]  * PML_tens[];
  epsilonr_annex[PMLsubs_xyz]  = eps_subs[]  * PML_tens[];
  epsilonr_annex[PMLlow_x]     = eps_low[]   * PML_tens[];
  epsilonr_annex[PMLlow_z]     = eps_low[]   * PML_tens[];
  epsilonr_annex[PMLlow_zx]    = eps_low[]   * PML_tens[];
  epsilonr_annex[PMLguide_z]   = eps_guide[] * PML_tens[];
  epsilonr_annex[PMLsuper_x]   = eps_super[] * PML_tens[];
  epsilonr_annex[PMLsuper_y]   = eps_super[] * PML_tens[];
  epsilonr_annex[PMLsuper_z]   = eps_super[] * PML_tens[];
  epsilonr_annex[PMLsuper_xy]  = eps_super[] * PML_tens[];
  epsilonr_annex[PMLsuper_zx]  = eps_super[] * PML_tens[];
  epsilonr_annex[PMLsuper_yz]  = eps_super[] * PML_tens[];
  epsilonr_annex[PMLsuper_xyz] = eps_super[] * PML_tens[];
  epsilonr_annex[Subs]         = eps_subs[]  * TensorDiag[1,1,1];
  epsilonr_annex[Low]          = eps_low[]   * TensorDiag[1,1,1];
  epsilonr_annex[Guide]        = eps_guide[] * TensorDiag[1,1,1];
  epsilonr_annex[Super]        = eps_super[] * TensorDiag[1,1,1];
  epsilonr_annex[Truc]         = eps_super[] * TensorDiag[1,1,1]; // debug
  epsilonr_annex[Box]          = eps_super[] * TensorDiag[1,1,1]; // debug

  List2D_E_x_re   = ListFromFile[StrCat[myDir, "Emode_grid_feed3D_x_re.dat"]];
  List2D_E_y_re   = ListFromFile[StrCat[myDir, "Emode_grid_feed3D_y_re.dat"]];
  List2D_E_z_re   = ListFromFile[StrCat[myDir, "Emode_grid_feed3D_z_re.dat"]];
  List2D_E_x_im   = ListFromFile[StrCat[myDir, "Emode_grid_feed3D_x_im.dat"]];
  List2D_E_y_im   = ListFromFile[StrCat[myDir, "Emode_grid_feed3D_y_im.dat"]];
  List2D_E_z_im   = ListFromFile[StrCat[myDir, "Emode_grid_feed3D_z_im.dat"]];
  List2D_H_x_re   = ListFromFile[StrCat[myDir, "Hmode_grid_feed3D_x_re.dat"]];
  List2D_H_y_re   = ListFromFile[StrCat[myDir, "Hmode_grid_feed3D_y_re.dat"]];
  List2D_H_z_re   = ListFromFile[StrCat[myDir, "Hmode_grid_feed3D_z_re.dat"]];
  List2D_H_x_im   = ListFromFile[StrCat[myDir, "Hmode_grid_feed3D_x_im.dat"]];
  List2D_H_y_im   = ListFromFile[StrCat[myDir, "Hmode_grid_feed3D_y_im.dat"]];
  List2D_H_z_im   = ListFromFile[StrCat[myDir, "Hmode_grid_feed3D_z_im.dat"]];
  List2D_Poy_x_re = ListFromFile[StrCat[myDir, "Poymode_grid_feed3D_x_re.dat"]];
  List2D_Poy_y_re = ListFromFile[StrCat[myDir, "Poymode_grid_feed3D_y_re.dat"]];
  List2D_Poy_z_re = ListFromFile[StrCat[myDir, "Poymode_grid_feed3D_z_re.dat"]];
  List2D_Poy_x_im = ListFromFile[StrCat[myDir, "Poymode_grid_feed3D_x_im.dat"]];
  List2D_Poy_y_im = ListFromFile[StrCat[myDir, "Poymode_grid_feed3D_y_im.dat"]];
  List2D_Poy_z_im = ListFromFile[StrCat[myDir, "Poymode_grid_feed3D_z_im.dat"]];
  Emode_interp2D_x_re[]   = InterpolationBilinear[$1,$2]{List2D_E_x_re()};
  Emode_interp2D_y_re[]   = InterpolationBilinear[$1,$2]{List2D_E_y_re()};
  Emode_interp2D_z_re[]   = InterpolationBilinear[$1,$2]{List2D_E_z_re()};
  Emode_interp2D_x_im[]   = InterpolationBilinear[$1,$2]{List2D_E_x_im()};
  Emode_interp2D_y_im[]   = InterpolationBilinear[$1,$2]{List2D_E_y_im()};
  Emode_interp2D_z_im[]   = InterpolationBilinear[$1,$2]{List2D_E_z_im()};
  Hmode_interp2D_x_re[]   = InterpolationBilinear[$1,$2]{List2D_H_x_re()};
  Hmode_interp2D_y_re[]   = InterpolationBilinear[$1,$2]{List2D_H_y_re()};
  Hmode_interp2D_z_re[]   = InterpolationBilinear[$1,$2]{List2D_H_z_re()};
  Hmode_interp2D_x_im[]   = InterpolationBilinear[$1,$2]{List2D_H_x_im()};
  Hmode_interp2D_y_im[]   = InterpolationBilinear[$1,$2]{List2D_H_y_im()};
  Hmode_interp2D_z_im[]   = InterpolationBilinear[$1,$2]{List2D_H_z_im()};
  Poymode_interp2D_x_re[] = InterpolationBilinear[$1,$2]{List2D_Poy_x_re()};
  Poymode_interp2D_y_re[] = InterpolationBilinear[$1,$2]{List2D_Poy_y_re()};
  Poymode_interp2D_z_re[] = InterpolationBilinear[$1,$2]{List2D_Poy_z_re()};
  Poymode_interp2D_x_im[] = InterpolationBilinear[$1,$2]{List2D_Poy_x_im()};
  Poymode_interp2D_y_im[] = InterpolationBilinear[$1,$2]{List2D_Poy_y_im()};
  Poymode_interp2D_z_im[] = InterpolationBilinear[$1,$2]{List2D_Poy_z_im()};

  gamma_mode[] = Complex[gamma_mode_re,gamma_mode_im];
  prop_z[]     = Complex[Cos[sigz*gamma_mode_re*Z[]],Sin[sigz*gamma_mode_re*Z[]]]*Exp[-sigz*gamma_mode_im*Z[]];
  sign_poy = 1; //debug

  // Emode2D_read[Surf_Inj]          =  ComplexVectorField[XYZ[]]{1};
  // Emode2D_read[#{Omega,-Surf_Inj}] = 1;
  // // Emode3D_read[]         =  Emode2D_read[X[],Y[],0] * prop_z[];
  // Emode3D_read[Omega]         =  Emode2D_read[$X,$Y,0];

  Emode3D[] = Vector[ Complex[Emode_interp2D_x_re[X[],Y[]],Emode_interp2D_x_im[X[],Y[]]],
                      Complex[Emode_interp2D_y_re[X[],Y[]],Emode_interp2D_y_im[X[],Y[]]],
                      Complex[Emode_interp2D_z_re[X[],Y[]],Emode_interp2D_z_im[X[],Y[]]]
                      ]*prop_z[];
  Hmode3D[] = Vector[ Complex[Hmode_interp2D_x_re[X[],Y[]],Hmode_interp2D_x_im[X[],Y[]]],
                      Complex[Hmode_interp2D_y_re[X[],Y[]],Hmode_interp2D_y_im[X[],Y[]]],
                      Complex[Hmode_interp2D_z_re[X[],Y[]],Hmode_interp2D_z_im[X[],Y[]]]
                      ]*prop_z[];
  Poymode3D[] = Vector[ Complex[Poymode_interp2D_x_re[X[],Y[]],Poymode_interp2D_x_im[X[],Y[]]],
                        Complex[Poymode_interp2D_y_re[X[],Y[]],Poymode_interp2D_y_im[X[],Y[]]],
                        Complex[Poymode_interp2D_z_re[X[],Y[]],Poymode_interp2D_z_im[X[],Y[]]]
                      ]*Exp[-2*sigz*gamma_mode_im*Z[]];

  source[] = (omega0/cel)^2*(epsilonr[]-epsilonr_annex[])*Emode3D[];
}



Constraint {
  { Name Dirichlet_PEC; Type Assign;
    Case {
      { Region Region[Surf_Diri]; Value 0.; }
    }
  }
}


Integration {
  { Name CurlCurl ;
    Case { 
      { Type Gauss ;
        Case {  
          { GeoElement Point       ; NumberOfPoints  1 ; }
          { GeoElement Line        ; NumberOfPoints  3 ; }
          { GeoElement Triangle    ; NumberOfPoints  4 ; }
          { GeoElement Tetrahedron ; NumberOfPoints  15 ; }
				}
      }
    }
  }
}


Jacobian {
  { Name Vol ;
    Case{
      { Region All ; Jacobian Vol ; }
    }
  }
  { Name Sur ;
    Case{
      { Region All ; Jacobian Sur ; }
    }
  }
  { Name Lin ;
    Case{
      { Region All ; Jacobian Lin ; }
    }
  }
}


FunctionSpace {
	{ Name H0curl; Type Form1;
	  BasisFunction {
	    { Name sn;  NameOfCoef un ; Function BF_Edge      ; Support Region[Omega]; Entity EdgesOf[All]; }
	    { Name sn2; NameOfCoef un2; Function BF_Edge_2E   ; Support Region[Omega]; Entity EdgesOf[All]; }
	    { Name sn3; NameOfCoef un3; Function BF_Edge_3F_b ; Support Region[Omega]; Entity FacetsOf[All]; }
	    { Name sn4; NameOfCoef un4; Function BF_Edge_3F_c ; Support Region[Omega]; Entity FacetsOf[All]; }
	    { Name sn5; NameOfCoef un5; Function BF_Edge_4E   ; Support Region[Omega]; Entity EdgesOf[All]; }
	  }
	 Constraint {
		If(flag_sym_diri==1)
		   { NameOfCoef un;  EntityType EdgesOf  ; NameOfConstraint Dirichlet_PEC; }
		   { NameOfCoef un2; EntityType EdgesOf  ; NameOfConstraint Dirichlet_PEC; }
		   { NameOfCoef un3; EntityType FacetsOf ; NameOfConstraint Dirichlet_PEC; }
		   { NameOfCoef un4; EntityType FacetsOf ; NameOfConstraint Dirichlet_PEC; }
		   { NameOfCoef un5; EntityType EdgesOf  ; NameOfConstraint Dirichlet_PEC; }
		 EndIf
	 }
	}
}

Formulation {
	{Name helmholtz3D; Type FemEquation;
		Quantity {
			{ Name u; Type Local; NameOfSpace H0curl;}
		}    
		Equation {
			Galerkin { [-1/mur[]*Dof{Curl u}             , {Curl u}]; In Omega; Jacobian Vol; Integration CurlCurl;  }
			Galerkin { [(omega0/cel)^2*epsilonr[]*Dof{u} , {u}     ]; In Omega; Jacobian Vol; Integration CurlCurl;  }
			Galerkin { [source[]                       , {u}     ]; In Omega; Jacobian Vol; Integration CurlCurl;  }
		}
	}
}

Resolution {
	{ Name res_helmholtz3D;
	  System {
	    { Name M; NameOfFormulation helmholtz3D; Type ComplexValue; Frequency Freq; }
	  }
	  Operation {
      // GmshRead[StrCat[myDir, "Emode_interest.pos"],1];
	    Generate[M];
			Solve[M]; 
			If(flag_light_out==0)
				SaveSolution[M];
			EndIf
	  }
	}
}

PostProcessing {
  { Name postpro_helmholtz3D; NameOfFormulation helmholtz3D;
    Quantity {
			{ Name epsilonr_xx     ; Value { Local { [CompXX[epsilonr[]]]; In Omega; Jacobian Vol; } } }
			{ Name epsilonr_yy     ; Value { Local { [CompYY[epsilonr[]]]; In Omega; Jacobian Vol; } } }
			{ Name epsilonr_zz     ; Value { Local { [CompZZ[epsilonr[]]]; In Omega; Jacobian Vol; } } }
			{ Name epsilonr_annex_xx     ; Value { Local { [CompXX[epsilonr_annex[]]]; In Omega; Jacobian Vol; } } }
			{ Name epsilonr_annex_yy     ; Value { Local { [CompYY[epsilonr_annex[]]]; In Omega; Jacobian Vol; } } }
			{ Name epsilonr_annex_zz     ; Value { Local { [CompZZ[epsilonr_annex[]]]; In Omega; Jacobian Vol; } } }
			{ Name source     ; Value { Local { [source[]]; In Omega; Jacobian Vol; } } }
			
			{ Name Emode3D    ; Value { Local { [Emode3D[] ]   ; In Omega; Jacobian Vol; } } }
			{ Name Hmode3D    ; Value { Local { [Hmode3D[] ]   ; In Omega; Jacobian Vol; } } }
			{ Name Poymode3D  ; Value { Local { [sign_poy*Poymode3D[] ] ; In Omega; Jacobian Vol; } } }
			
			{ Name E_dif         ; Value { Local { [{u}]; In Omega; Jacobian Vol; } } }
			{ Name epsrE_dif    ; Value { Local { [epsilonr[]*{u}]; In Omega; Jacobian Vol; } } }
			{ Name epsrE_tot    ; Value { Local { [epsilonr[]*({u}+Emode3D[])]; In Omega; Jacobian Vol; } } }
			{ Name epsrE_inc    ; Value { Local { [epsilonr[]*Emode3D[]]; In Omega; Jacobian Vol; } } }
			{ Name H_dif         ; Value { Local { [siwt*I[]/(mur[]*mu0*omega0)*norm_dint*{Curl u}]; In Omega; Jacobian Vol; } } }
			{ Name E_tot         ; Value { Local { [{u}+Emode3D[]]; In Omega; Jacobian Vol; } } }
			{ Name H_tot         ; Value { Local { [ siwt*I[]/(mur[]*mu0*omega0)*norm_dint*{Curl u} +Hmode3D[]]; In Omega; Jacobian Vol; } } }
			{ Name Poy_dif       ; Value { Local { [  sign_poy*0.5*Re[Cross[{u}           , Conj[          siwt*I[]/(mur[]*mu0*omega0)*norm_dint*{Curl u}]]]  ]; In Omega; Jacobian Vol; } } }
			{ Name Poy_tot       ; Value { Local { [  sign_poy*0.5*Re[Cross[{u}+Emode3D[] , Conj[Hmode3D[]+siwt*I[]/(mur[]*mu0*omega0)*norm_dint*{Curl u}]]]  ]; In Omega; Jacobian Vol; } } }
			{ Name Poy_inc       ; Value { Local { [  sign_poy*0.5*Re[Cross[Emode3D[]     , Conj[Hmode3D[]                                               ]]]  ]; In Omega; Jacobian Vol; } } }
			{ Name Poy_tot_1EdHd ; Value { Local { [  sign_poy*0.5*Re[Cross[{u}           , Conj[          siwt*I[]/(mur[]*mu0*omega0)*norm_dint*{Curl u}]]]  ]; In Omega; Jacobian Vol; } } }
			{ Name Poy_tot_2EiHi ; Value { Local { [  sign_poy*0.5*Re[Cross[Emode3D[]     , Conj[Hmode3D[]                                               ]]]  ]; In Omega; Jacobian Vol; } } }
			{ Name Poy_tot_3EdHi ; Value { Local { [  sign_poy*0.5*Re[Cross[{u}           , Conj[Hmode3D[]                                               ]]]  ]; In Omega; Jacobian Vol; } } }
			{ Name Poy_tot_4EiHd ; Value { Local { [  sign_poy*0.5*Re[Cross[Emode3D[]     , Conj[          siwt*I[]/(mur[]*mu0*omega0)*norm_dint*{Curl u}]]]  ]; In Omega; Jacobian Vol; } } }

			{ Name Poynting_mode_in    ; Value { Integral { [ sign_poy*0.5*Re[Cross[Emode3D[],Conj[ Hmode3D[] ] ]] ]; In Omega; Integration CurlCurl; Jacobian Sur; } } }
			{ Name Poynting_dif_out_zm ; Value { Integral { [ sign_poy*0.5*Re[Cross[{u},Conj[ siwt*I[]/(mur[]*mu0*omega0)*norm_dint*{Curl u}]] ] ]; In Omega; Integration CurlCurl; Jacobian Sur; } } }
			{ Name Poynting_tot_out_zp ; Value { Integral { [ sign_poy*0.5*Re[Cross[({u}+Emode3D[]),Conj[Hmode3D[]+siwt*I[]/(mur[]*mu0*omega0)*norm_dint*{Curl u}]]] ]; In Omega; Integration CurlCurl; Jacobian Sur; } } }
			
			// // calculate poynting tot and 0
			// { Name Poynting_mode_in  ; Value { Integral   { [ sign_poy*0.5*CompZ[Re[Cross[Emode3D[],Conj[ Hmode3D[] ] ]] ]]; In Surf_leak_guide_zp; Integration CurlCurl; Jacobian Sur; } } }
			// { Name Poynting_mode_out ; Value { Integral   { [ sign_poy*0.5*CompZ[Re[Cross[Emode3D[],Conj[ Hmode3D[] ] ]] ]]; In Surf_leak_guide_zm; Integration CurlCurl; Jacobian Sur; } } }
			// { Name Poynting_dif_out_zp ; Value { Integral { [ sign_poy*0.5*CompZ[Re[Cross[{u},Conj[ siwt*I[]/(mur[]*mu0*omega0)*norm_dint*{Curl u}]] ] ]]; In Surf_leak_guide_zp; Integration CurlCurl; Jacobian Sur; } } }
			// { Name Poynting_dif_out_zm ; Value { Integral { [ sign_poy*0.5*CompZ[Re[Cross[{u},Conj[ siwt*I[]/(mur[]*mu0*omega0)*norm_dint*{Curl u}]] ] ]]; In Surf_leak_guide_zm; Integration CurlCurl; Jacobian Sur; } } }
			// { Name Poynting_tot_out_zp ; Value { Integral { [ sign_poy*0.5*CompZ[Re[Cross[({u}+Emode3D[]),Conj[Hmode3D[]+siwt*I[]/(mur[]*mu0*omega0)*norm_dint*{Curl u}]]] ]]; In Surf_leak_guide_zp; Integration CurlCurl; Jacobian Sur; } } }
			// { Name Poynting_tot_out_zm ; Value { Integral { [ sign_poy*0.5*CompZ[Re[Cross[({u}+Emode3D[]),Conj[Hmode3D[]+siwt*I[]/(mur[]*mu0*omega0)*norm_dint*{Curl u}]]] ]]; In Surf_leak_guide_zm; Integration CurlCurl; Jacobian Sur; } } }
			{ Name normE_tot         ; Value { Local    { [Norm[{u}+Emode3D[]]+plotlog_offset]; In Omega;Jacobian Vol; } } }
			{ Name normE_tot2        ; Value { Local    { [SquNorm[{u}+Emode3D[]]]; In Omega;Jacobian Vol; } } }
			{ Name normE_dif         ; Value { Local    { [Norm[{u}]+plotlog_offset]; In Omega;Jacobian Vol; } } }
			{ Name normE_diflog10    ; Value { Local    { [Log10[Norm[{u}]+plotlog_offset]]; In Omega;Jacobian Vol; } } }
			{ Name normEmode3D       ; Value { Local    { [Norm[Emode3D[]]]; In Omega;Jacobian Vol; } } }
			// { Name Emode2D_read      ; Value { Local    { [Emode2D_read[]]; In Surf_Inj ; Jacobian Sur; } } }
			// { Name Emode3D_read      ; Value { Local    { [Emode3D_read[]]; In Domain ; Jacobian Vol; } } }
			{ Name int_normE2_tot    ; Value { Integral { [ epsilon0*omega0 * 0.5*Fabs[Im[CompXX[epsilonr[]]]]*(SquNorm[{u}+Emode3D[]]) ]; In Truc ; Integration CurlCurl; Jacobian Vol; } } }
			{ Name int_normE2_tot_box; Value { Integral { [ (SquNorm[{u}+Emode3D[]]) ]; In Box  ; Integration CurlCurl; Jacobian Vol; } } }
			{ Name vol_box           ; Value { Integral { [ 1. ]; In Box  ; Integration CurlCurl; Jacobian Vol; } } }
    }
  }
}

PostOperation {
  { Name postop_helmholtz3D; NameOfPostProcessing postpro_helmholtz3D;
      Operation {
        Print[ E_tot         , OnElementsOf Domain, File StrCat[myDir, "./dim3D_E_tot.pos"]];
        Print[ Emode3D       , OnElementsOf Domain, File StrCat[myDir, "./dim3D_Emode3D.pos"]];
        // Print[ Emode2D_read  , OnElementsOf Surf_Inj, File StrCat[myDir, "./Emode2D_read.pos"]];
        // Print[ Emode3D_read  , OnElementsOf Domain   , File StrCat[myDir, "./Emode3D_read.pos"]];
        If(flag_light_out==0)
          // Print[ epsilonr_xx   , OnElementsOf Omega , File StrCat[myDir, "./dim3D_epsilonr_xx.pos"]];
          // Print[ epsilonr_yy   , OnElementsOf Omega , File StrCat[myDir, "./dim3D_epsilonr_yy.pos"]];
          // Print[ epsilonr_zz   , OnElementsOf Omega , File StrCat[myDir, "./dim3D_epsilonr_zz.pos"]];
          // Print[ epsilonr_annex_xx   , OnElementsOf Omega , File StrCat[myDir, "./dim3D_epsilonr_annex_xx.pos"]];
          // Print[ epsilonr_annex_yy   , OnElementsOf Omega , File StrCat[myDir, "./dim3D_epsilonr_annex_yy.pos"]];
          // Print[ epsilonr_annex_zz   , OnElementsOf Omega , File StrCat[myDir, "./dim3D_epsilonr_annex_zz.pos"]];
          // Print[ source        , OnElementsOf Omega , File StrCat[myDir, "./dim3D_source.pos"]];
  	      Print[ Poymode3D     , OnElementsOf Omega , File StrCat[myDir, "./dim3D_Poymode3D.pos"]];
  	      Print[ Emode3D       , OnElementsOf Omega , File StrCat[myDir, "./dim3D_Emode3D.pos"]];
  	      Print[ Hmode3D       , OnElementsOf Omega , File StrCat[myDir, "./dim3D_Hmode3D.pos"]];
  	      Print[ normE_tot2    , OnElementsOf Truc  , File StrCat[myDir, "./dim3D_normE_tot2_rods.pos"]];
  	      Print[ E_dif         , OnElementsOf Omega , File StrCat[myDir, "./dim3D_E_dif.pos"]];
  	      Print[ Poy_tot       , OnElementsOf Omega , File StrCat[myDir, "./dim3D_Poy_tot.pos"]];
  	      Print[ H_dif         , OnElementsOf Omega , File StrCat[myDir, "./dim3D_H_dif.pos"]];
          // Print[ Poy_dif       , OnElementsOf Omega , File StrCat[myDir, "./dim3D_Poy_dif.pos"]];
          // Print[ normE_dif     , OnElementsOf Omega , File StrCat[myDir, "./dim3D_normE_dif.pos"]];
          // Print[ normE_diflog10, OnElementsOf Omega , File StrCat[myDir, "./dim3D_normE_diflog10.pos"]];
          // Print[ E_tot         , OnElementsOf Domain, File StrCat[myDir, "./dim3D_E_tot.pos"]];
          // Print[ H_tot         , OnElementsOf Domain, File StrCat[myDir, "./dim3D_H_tot.pos"]];
          // Print[ Poy_tot       , OnElementsOf Domain, File StrCat[myDir, "./dim3D_Poy_tot.pos"]];
          // Print[ normE_tot     , OnElementsOf Domain, File StrCat[myDir, "./dim3D_normE_tot.pos"]];
          // Print[ Poy_tot_1EdHd , OnElementsOf Domain, File StrCat[myDir, "./dim3D_Poy_tot_1EdHd.pos"]];
          // Print[ Poy_tot_2EiHi , OnElementsOf Domain, File StrCat[myDir, "./dim3D_Poy_tot_2EiHi.pos"]];
          // Print[ Poy_tot_3EdHi , OnElementsOf Domain, File StrCat[myDir, "./dim3D_Poy_tot_3EdHi.pos"]];
          // Print[ Poy_tot_4EiHd , OnElementsOf Domain, File StrCat[myDir, "./dim3D_Poy_tot_4EiHd.pos"]];
  				Print[ Poy_tot, OnGrid { $A, $B, z_export_poy3Dp_min}
  									{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
  										{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
  										{z_export_poy3Dp_min} }, 
  										File StrCat[myDir, Sprintf("dim3D_cut_Poy_tot_grid_zp_%g.pos",0)]];
  				Print[ Poy_tot_3EdHi, OnGrid { $A, $B, z_export_poy3Dm_min}
  									{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
  										{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
  										{z_export_poy3Dm_min} },
  										File StrCat[myDir, Sprintf("dim3D_cut_Poy_tot_3EdHi_grid_zm_%g.pos",0)]];
  				Print[ Poy_tot_3EdHi, OnGrid { $A, $B, z_export_poy3Dp_min}
  									{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
  										{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
  										{z_export_poy3Dp_min} },
  										File StrCat[myDir, Sprintf("dim3D_cut_Poy_tot_3EdHi_grid_zp_%g.pos",0)]];
  				Print[ Poy_tot_4EiHd, OnGrid { $A, $B, z_export_poy3Dm_min}
  									{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
  										{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
  										{z_export_poy3Dm_min} },
  										File StrCat[myDir, Sprintf("dim3D_cut_Poy_tot_4EiHd_grid_zm_%g.pos",0)]];
  				Print[ Poy_tot_4EiHd, OnGrid { $A, $B, z_export_poy3Dp_min}
  									{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
  										{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
  										{z_export_poy3Dp_min} },
  										File StrCat[myDir, Sprintf("dim3D_cut_Poy_tot_4EiHd_grid_zp_%g.pos",0)]];
  				Print[ E_tot, OnGrid { $A, h_truc+dist2truc_cuty, $C}
  									{ {x_export3Dm:x_export3Dp+delta_interp : (x_export3Dp-x_export3Dm)/(npts_export3D_x-1)},
  										{h_truc+dist2truc_cuty}, 
  										{z_export3Dm:z_export3Dp+delta_interp : (z_export3Dp-z_export3Dm)/(npts_export3D_z-1)} }, 
  										File StrCat[myDir, "cut_E_tot_grid_top.pos"]];
  				Print[ E_tot, OnGrid { $A, h_truc/2, $C}
  									{ {x_export3Dm:x_export3Dp+delta_interp : (x_export3Dp-x_export3Dm)/(npts_export3D_x-1)},
  										{h_truc/2},
  										{z_export3Dm:z_export3Dp+delta_interp : (z_export3Dp-z_export3Dm)/(npts_export3D_z-1)} }, 
  										File StrCat[myDir, "cut_E_tot_grid_mid.pos"]];
  				Print[ E_tot, OnGrid { $A, -dist2truc_cuty, $C}
  									{ {x_export3Dm:x_export3Dp+delta_interp : (x_export3Dp-x_export3Dm)/(npts_export3D_x-1)},
  										{-dist2truc_cuty}, 
  										{z_export3Dm:z_export3Dp+delta_interp : (z_export3Dp-z_export3Dm)/(npts_export3D_z-1)} }, 
  										File StrCat[myDir, "cut_E_tot_grid_bot.pos"]];			
        	Print[ E_dif, OnGrid { $A, $B, z_export_poy3Dm_min} 
        	          { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
        		          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
        		          {z_export_poy3Dm_min} }, 
        	          File StrCat[myDir, "E_dif_2expand.pos"]];
        	Print[ E_tot, OnGrid { $A, $B, z_export_poy3Dp_max} 
        	          { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
        		          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
        		          {z_export_poy3Dp_max} }, 
        	          File StrCat[myDir, "E_tot_2expand.pos"]];
        	Print[ epsrE_dif, OnGrid { $A, $B, z_export_poy3Dm_min} 
        	          { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
        		          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
        		          {z_export_poy3Dm_min} }, 
        	          File StrCat[myDir, "epsrE_dif_2expand.pos"]];
        	Print[ epsrE_tot, OnGrid { $A, $B, z_export_poy3Dp_max} 
        	          { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
        		          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
        		          {z_export_poy3Dp_max} }, 
        	          File StrCat[myDir, "epsrE_tot_2expand.pos"]];
			EndIf
			For k In {0:npts_export_poy3D_z-1}
				Print[ Poy_tot, OnGrid { $A, $B, z_export_poy3Dp_min}
									{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
										{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
										{z_export_poy3Dp_min} }, 
										File StrCat[myDir, Sprintf("cut_Poy_tot_grid_zp_%g.txt",k)],Format Table];
				Print[ Poy_tot, OnGrid { $A, $B, z_export_poy3Dm_min}
									{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
										{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
										{z_export_poy3Dm_min} }, 
										File StrCat[myDir, Sprintf("cut_Poy_tot_grid_zm_%g.txt",k)],Format Table];			
				Print[ Poy_dif, OnGrid { $A, $B, z_export_poy3Dp_min}
									{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
										{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
										{z_export_poy3Dp_min} }, 
										File StrCat[myDir, Sprintf("cut_Poy_dif_grid_zp_%g.txt",k)],Format Table];
				Print[ Poy_dif, OnGrid { $A, $B, z_export_poy3Dm_min}
									{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
										{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
										{z_export_poy3Dm_min} }, 
										File StrCat[myDir, Sprintf("cut_Poy_dif_grid_zm_%g.txt",k)],Format Table];			
				Print[ Poy_inc, OnGrid { $A, $B, z_export_poy3Dm_min}
									{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
										{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
										{z_export_poy3Dm_min} }, 
										File StrCat[myDir, Sprintf("cut_Poymode3D_grid_zm_%g.txt",k)],Format Table];			
				// Print[ Poy_tot_2EiHi, OnGrid { $A, $B, z_export_poy3Dm_min}
				// 					{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
				// 						{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
				// 						{z_export_poy3Dm_min} }, 
				// 						File StrCat[myDir, Sprintf("cut_Poymode3D_grid_zm_%g.txt",k)],Format Table];
				// Print[ Poy_tot_3EdHi, OnGrid { $A, $B, z_export_poy3Dp_min}
				// 					{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
				// 						{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
				// 						{z_export_poy3Dp_min} },
				// 						File StrCat[myDir, Sprintf("cut_Poy_tot_3EdHi_grid_zp_%g.txt",k)],Format Table];
				// Print[ Poy_tot_3EdHi, OnGrid { $A, $B, z_export_poy3Dm_min}
				// 					{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
				// 						{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
				// 						{z_export_poy3Dm_min} },
				// 						File StrCat[myDir, Sprintf("cut_Poy_tot_3EdHi_grid_zm_%g.txt",k)],Format Table];
				// Print[ Poy_tot_4EiHd, OnGrid { $A, $B, z_export_poy3Dp_min}
				// 					{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
				// 						{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
				// 						{z_export_poy3Dp_min} },
				// 						File StrCat[myDir, Sprintf("cut_Poy_tot_4EiHd_grid_zp_%g.txt",k)],Format Table];
				// Print[ Poy_tot_4EiHd, OnGrid { $A, $B, z_export_poy3Dm_min}
				// 					{ {x_export_poy3Dm:x_export_poy3Dp+delta_interp : (x_export_poy3Dp-x_export_poy3Dm)/(npts_export_poy3D_x-1)},
				// 						{y_export_poy3Dm:y_export_poy3Dp+delta_interp : (y_export_poy3Dp-y_export_poy3Dm)/(npts_export_poy3D_y-1)},
				// 						{z_export_poy3Dm_min} },
				// 						File StrCat[myDir, Sprintf("cut_Poy_tot_4EiHd_grid_zm_%g.txt",k)],Format Table];
				
			EndFor
      // for expansion
    	Print[ E_dif, OnGrid { $A, $B, z_export_poy3Dm_min} 
    	          { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
    		          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
    		          {z_export_poy3Dm_min} }, 
    	          File StrCat[myDir, "E_dif_2expand.txt"],Format Table];
    	Print[ E_tot, OnGrid { $A, $B, z_export_poy3Dp_max} 
    	          { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
    		          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
    		          {z_export_poy3Dp_max} }, 
    	          File StrCat[myDir, "E_tot_2expand.txt"],Format Table];
    	// Print[ epsrE_dif, OnGrid { $A, $B, z_export_poy3Dm_min} 
    	//           { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
    	// 	          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
    	// 	          {z_export_poy3Dm_min} }, 
    	//           File StrCat[myDir, "epsrE_dif_2expand.txt"],Format Table];
    	// Print[ epsrE_tot, OnGrid { $A, $B, z_export_poy3Dp_max}
    	//           { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
    	// 	          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
    	// 	          {z_export_poy3Dp_max} }, 
    	//           File StrCat[myDir, "epsrE_tot_2expand.txt"],Format Table];
    	// Print[ epsrE_inc, OnGrid { $A, $B, z_export_poy3Dp_max}
    	//           { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
    	// 	          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
    	// 	          {z_export_poy3Dp_max} }, 
    	//           File StrCat[myDir, "epsrE_inc_2expand.txt"],Format Table];
    	// // Print[ Emode3D, OnGrid { $A, $B, z_export_poy3Dp_max}
    	// //           { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
    	// // 	          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
    	// // 	          {z_export_poy3Dp_max} }, 
    	// //           File StrCat[myDir, "E_inc_2expand.txt"],Format Table];
    	Print[ Emode3D, OnGrid { $A, $B, 0}
    	          { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
    		          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
    		          {0} }, 
    	          File StrCat[myDir, "E_inc_2expand.txt"],Format Table];
    	// Print[ epsrE_inc, OnGrid { $A, $B, z_export_poy3Dp_max}
    	//           { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
    	// 	          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
    	// 	          {z_export_poy3Dp_max} }, 
    	//           File StrCat[myDir, "epsrE_inc_2expand.pos"]];
    // 	Print[ Emode3D, OnGrid { $A, $B, 0}
    // 	          { {x_export2Dm : x_export2Dp+delta_interp : (x_export2Dp-x_export2Dm)/(npts_export2D_x-1)}, 
    // 		          {y_export2Dm : y_export2Dp+delta_interp : (y_export2Dp-y_export2Dm)/(npts_export2D_y-1)}, 
    // 		          {0} }, 
    // 	          File StrCat[myDir, "E_inc_2expand.pos"]];
    //   // endfor expansion
      
			// Print[ E_tot, OnGrid { $A, h_truc+dist2truc_cuty, $C}
			// 					{ {x_export3Dm:x_export3Dp+delta_interp : (x_export3Dp-x_export3Dm)/(npts_export3D_x-1)},
			// 						{h_truc+dist2truc_cuty}, 
			// 						{z_export3Dm:z_export3Dp+delta_interp : (z_export3Dp-z_export3Dm)/(npts_export3D_z-1)} }, 
			// 						File StrCat[myDir, "cut_E_tot_grid_top.txt"],Format Table];
			// Print[ E_tot, OnGrid { $A, h_truc/2, $C}
			// 					{ {x_export3Dm:x_export3Dp+delta_interp : (x_export3Dp-x_export3Dm)/(npts_export3D_x-1)},
			// 						{h_truc/2},
			// 						{z_export3Dm:z_export3Dp+delta_interp : (z_export3Dp-z_export3Dm)/(npts_export3D_z-1)} }, 
			// 						File StrCat[myDir, "cut_E_tot_grid_mid.txt"],Format Table];
			// Print[ E_tot, OnGrid { $A, -dist2truc_cuty, $C}
			// 					{ {x_export3Dm:x_export3Dp+delta_interp : (x_export3Dp-x_export3Dm)/(npts_export3D_x-1)},
			// 						{-dist2truc_cuty},
			// 						{z_export3Dm:z_export3Dp+delta_interp : (z_export3Dp-z_export3Dm)/(npts_export3D_z-1)} }, 
			// 						File StrCat[myDir, "cut_E_tot_grid_bot.txt"],Format Table];
			Print[ int_normE2_tot[Truc], OnGlobal, Format Table, File StrCat[myDir, "int_normE2_tot.txt"]];
			Print[ int_normE2_tot_box[Box], OnGlobal, Format Table, File StrCat[myDir, "int_normE2_tot_box.txt"]];			
			Print[ vol_box[Box], OnGlobal, Format Table, File StrCat[myDir, "vol_box.txt"]];			
    }
  }
}

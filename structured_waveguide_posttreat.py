#%%
import numpy as np
import pylab as pl

res = np.load('parametric_rl-rt-h.npz')
tab_d_truc = res['tab_d_truc']
tab_h_truc = res['tab_h_truc']
tab_rl_truc = res['tab_rl_truc']
tab_rt_truc = res['tab_rt_truc']
tab_N_truc = res['tab_N_truc']
T_in_guide = res['T_in_guide']
R_in_guide = res['R_in_guide']
A_in_guide = res['A_in_guide']
eigs3D = np.load('paper_eigs3D.npz')['eigs3D']
print(eigs3D[1])
d_truc = tab_d_truc.item()
an_t=res['an_t']
an_r=res['an_r']

pl.figure()
pl.plot(tab_N_truc,T_in_guide[:,0,0,0,0],'o-',label='T direct')
pl.plot(tab_N_truc,np.abs(an_t[:,0,0,0,0,1])**2,'o-',label='$|a_1|$')
pl.plot(tab_N_truc,np.sum(np.abs(an_t[:,0,0,0,0,:])**2,axis=1),'o-',label='$|\sum a_n|$')

pl.plot(tab_N_truc,T_in_guide[0,0,0,0,0]**(tab_N_truc),'o-',label='T extrap')
pl.plot(tab_N_truc,T_in_guide[0,0,0,0,0]*np.exp(-2*eigs3D[1].imag*(tab_N_truc-1)*d_truc),'o-',label="$e^{-2i\gamma'' x}}$")
pl.legend()
pl.xlim([0,14])
pl.ylim([0,1])
pl.xlabel('Number of periods')
pl.ylabel('Fraction of incident energy')
pl.savefig('compare_modal_direct_9p2i.pdf')

# pl.plot(tab_N_truc[1::2],T_in_guide[1,0,0,0,0]**(tab_N_truc[1::2]-1),'o-')
# pl.plot(tab_N_truc[2::2],T_in_guide[1,0,0,0,0]**(tab_N_truc[2::2]-2),'o-')

pl.show()


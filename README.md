# structured_waveguides
Details of this model are available at https://arxiv.org/abs/1907.11540

To run the models, you need :
- python v>=3.5 with numpy and matplotlib
- a recent version of Gmsh/GetDP. Just download the ONELAB software bundle from http://onelab.info/, unzip and Gmsh/GetDP will at the root of the resulting folder
- replace the absolute paths to Gmsh and GetDP in "structured_waveguide.py" lines 20-21 (that is the path to the ONELAB folder, see previous step)
- just run in a terminal "sh run_structured_waveguide.sh" from the same location as the model files

These scripts were used to produce most of the figures of this paper.

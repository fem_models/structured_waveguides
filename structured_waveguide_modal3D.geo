SetFactory("OpenCASCADE");
Include "parameters_gmsh_getdp.dat";

// flag_3D  = 1;
// flag_sym = 1;

// set embedding layer to 0 for now
h_embed      = 0;
eps_embed_re = eps_super;
eps_embed_im = 0;

lc_super       = lambda0/(paramaille3Dmodal*Sqrt[eps_super]);
lc_embed       = lambda0/(paramaille3Dmodal*Sqrt[eps_embed_re]);
lc_low         = lambda0/(paramaille3Dmodal*Sqrt[eps_low]);
lc_guide       = lambda0/(paramaille3Dmodal*Sqrt[eps_guide]);
lc_subs        = lambda0/(paramaille3Dmodal*Sqrt[eps_subs]);
lc_truc        = lambda0/(paramaille3Dmodal*Sqrt[Fabs[eps_truc_re]]);
lc_PML_super   = lc_super *1.5;
lc_PML_embed   = lc_embed *1.5;
lc_PML_low     = lc_low   *1.5;
lc_PML_subs    = lc_subs  *3;
safe_bool_subs = h_pmllat;
Coherence;

If(flag_3D==0)
  If(flag_sym==1)
    Rectangle(1)   = {0                        ,            -h_pmlbot-h_subs-h_low, -d_truc/2 , (space_pml_x+w_guide/2.) , h_pmlbot};
    Rectangle(2)   = {0                        ,                     -h_subs-h_low, -d_truc/2 , (space_pml_x+w_guide/2.) , h_subs};
    Rectangle(3)   = {0                        ,                            -h_low, -d_truc/2 , (space_pml_x+w_guide/2.) , h_low};
    Rectangle(4)   = {0                        ,                                 0, -d_truc/2 , (space_pml_x+w_guide/2.) , (h_guide+h_above+h_truc)};
    Rectangle(6)   = {0                        , h_above+(h_guide+h_embed+h_truc) , -d_truc/2 , (space_pml_x+w_guide/2.) , h_pmltop};
    Rectangle(13)  = {(space_pml_x+w_guide/2.) ,            -h_pmlbot-h_subs-h_low, -d_truc/2 ,                 h_pmllat , h_pmlbot};
    Rectangle(14)  = {(space_pml_x+w_guide/2.) ,                     -h_subs-h_low, -d_truc/2 ,                 h_pmllat , h_subs};
    Rectangle(15)  = {(space_pml_x+w_guide/2.) ,                            -h_low, -d_truc/2 ,                 h_pmllat , h_low};
    Rectangle(16)  = {(space_pml_x+w_guide/2.) ,                                 0, -d_truc/2 ,                 h_pmllat , (h_guide+h_above+h_truc)};
    Rectangle(18)  = {(space_pml_x+w_guide/2.) , h_above+(h_guide+h_embed+h_truc) , -d_truc/2 ,                 h_pmllat , h_pmltop};
    Rectangle(19)  = {0.                       ,                                0 , -d_truc/2 ,                 w_guide/2, h_guide};
    Coherence;

    Physical Surface("SUBS",1)         = {2};
    Physical Surface("LOW",2)          = {3};
    Physical Surface("GUIDE",3)        = {19};
    Physical Surface("SUPER",5)        = {20};
    Physical Surface("PMLSUBS_X",6)    = {14};
    Physical Surface("PMLSUBS_Y",7)    = {1};
    Physical Surface("PMLSUBS_XY",8)   = {13};
    Physical Surface("PMLLOW_X",9)     = {15};
    Physical Surface("PMLSUPER_X",11)  = {16};
    Physical Surface("PMLSUPER_Y",12)  = {6};
    Physical Surface("PMLSUPER_XY",13) = {18};
    Physical Surface("TRUC",14)        = {};
    Physical Surface("BLOCH_ZM",101)   = {};
    Physical Surface("BLOCH_ZP",102)   = {};
    Physical Line("SURF_SYM_X0",110)   = {10,13,16,17,3,20};
    Physical Line("SURF_DIRI_OUT",120) = {7,21,22,24,26,28,30,31,19};
    Physical Point("PRINT_POINT",1000) = {1};
  Else
    Rectangle(1)   = {-(space_pml_x+w_guide/2.) ,            -h_pmlbot-h_subs-h_low, -d_truc/2 , 2*(space_pml_x+w_guide/2.),h_pmlbot};
    Rectangle(2)   = {-(space_pml_x+w_guide/2.) ,                     -h_subs-h_low, -d_truc/2 , 2*(space_pml_x+w_guide/2.),h_subs};
    Rectangle(3)   = {-(space_pml_x+w_guide/2.) ,                            -h_low, -d_truc/2 , 2*(space_pml_x+w_guide/2.),h_low};
    Rectangle(4)   = {-(space_pml_x+w_guide/2.) ,                                 0, -d_truc/2 , 2*(space_pml_x+w_guide/2.),(h_guide+h_above+h_truc)};
    Rectangle(6)   = {-(space_pml_x+w_guide/2.) , h_above+(h_guide+h_embed+h_truc) , -d_truc/2 , 2*(space_pml_x+w_guide/2.),h_pmltop};

    Rectangle(7)   = {-(h_pmllat+space_pml_x+w_guide/2.) ,            -h_pmlbot-h_subs-h_low, -d_truc/2 , h_pmllat,h_pmlbot};
    Rectangle(8)   = {-(h_pmllat+space_pml_x+w_guide/2.) ,                     -h_subs-h_low, -d_truc/2 , h_pmllat,h_subs};
    Rectangle(9)   = {-(h_pmllat+space_pml_x+w_guide/2.) ,                            -h_low, -d_truc/2 , h_pmllat,h_low};
    Rectangle(10)  = {-(h_pmllat+space_pml_x+w_guide/2.) ,                                 0, -d_truc/2 , h_pmllat,(h_guide+h_above+h_truc)};
    Rectangle(12)  = {-(h_pmllat+space_pml_x+w_guide/2.) , h_above+(h_guide+h_embed+h_truc) , -d_truc/2 , h_pmllat,h_pmltop};

    Rectangle(13)  = {(space_pml_x+w_guide/2.) ,            -h_pmlbot-h_subs-h_low, -d_truc/2 , h_pmllat,h_pmlbot};
    Rectangle(14)  = {(space_pml_x+w_guide/2.) ,                     -h_subs-h_low, -d_truc/2 , h_pmllat,h_subs};
    Rectangle(15)  = {(space_pml_x+w_guide/2.) ,                            -h_low, -d_truc/2 , h_pmllat,h_low};
    Rectangle(16)  = {(space_pml_x+w_guide/2.) ,                                 0, -d_truc/2 , h_pmllat,(h_guide+h_above+h_truc)};
    Rectangle(18)  = {(space_pml_x+w_guide/2.) , h_above+(h_guide+h_embed+h_truc) , -d_truc/2 , h_pmllat,h_pmltop};

    Rectangle(19)  = {-w_guide/2. ,    0, -d_truc/2 , w_guide,h_guide};

    Coherence;
    Physical Surface("SUBS",1)         = {2};
    Physical Surface("LOW",2)          = {3};
    Physical Surface("GUIDE",3)        = {19};
    Physical Surface("SUPER",5)        = {20};
    Physical Surface("PMLSUBS_X",6)    = {14,8};
    Physical Surface("PMLSUBS_Y",7)    = {1};
    Physical Surface("PMLSUBS_XY",8)   = {13,7};
    Physical Surface("PMLLOW_X",9)     = {15,9};
    Physical Surface("PMLSUPER_X",11)  = {16,10};
    Physical Surface("PMLSUPER_Y",12)  = {6};
    Physical Surface("PMLSUPER_XY",13) = {18,12};
    Physical Surface("TRUC",14)        = {};
    Physical Surface("BLOCH_ZM",101)   = {};
    Physical Surface("BLOCH_ZP",102)   = {};
    Physical Line("SURF_SYM_X0",110)   = {};
    Physical Line("SURF_DIRI_OUT",120) = {82,84,86,88,90,92,91,80,103,102,100,98,96,94,93,70};
    Physical Point("PRINT_POINT",1000) = {1};
  EndIf

  Characteristic Length{PointsOf{Physical Surface{11};}} = lc_PML_super;
  Characteristic Length{PointsOf{Physical Surface{12};}} = lc_PML_super;
  Characteristic Length{PointsOf{Physical Surface{13};}} = lc_PML_super;
  Characteristic Length{PointsOf{Physical Surface{10};}} = lc_PML_embed;
  Characteristic Length{PointsOf{Physical Surface{9};}}  = lc_PML_low;
  Characteristic Length{PointsOf{Physical Surface{6};}}  = lc_PML_subs;
  Characteristic Length{PointsOf{Physical Surface{7};}}  = lc_PML_subs;
  Characteristic Length{PointsOf{Physical Surface{8};}}  = lc_PML_subs;
  Characteristic Length{PointsOf{Physical Surface{5};}}  = lc_super;
  Characteristic Length{PointsOf{Physical Surface{4};}}  = lc_embed;
  Characteristic Length{PointsOf{Physical Surface{2};}}  = lc_low;
  Characteristic Length{PointsOf{Physical Surface{3};}}  = lc_guide;
  Characteristic Length{PointsOf{Physical Surface{1};}}  = lc_subs;
  Characteristic Length{PointsOf{Physical Surface{14};}} = lc_truc;
Else
  Rectangle(1)   = {-(space_pml_x+w_guide/2.) ,            -h_pmlbot-h_subs-h_low, -d_truc/2 , 2*(space_pml_x+w_guide/2.),h_pmlbot};
  Rectangle(2)   = {-(space_pml_x+w_guide/2.) ,                     -h_subs-h_low, -d_truc/2 , 2*(space_pml_x+w_guide/2.),h_subs};
  Rectangle(3)   = {-(space_pml_x+w_guide/2.) ,                            -h_low, -d_truc/2 , 2*(space_pml_x+w_guide/2.),h_low};
  Rectangle(4)   = {-(space_pml_x+w_guide/2.) ,                                 0, -d_truc/2 , 2*(space_pml_x+w_guide/2.),(h_guide+h_above+h_truc)};
  Rectangle(5)   = {-(space_pml_x+w_guide/2.) , h_above+(h_guide+h_embed+h_truc) , -d_truc/2 , 2*(space_pml_x+w_guide/2.),h_pmltop};

  Rectangle(6)   = {-(h_pmllat+space_pml_x+w_guide/2.) ,            -h_pmlbot-h_subs-h_low, -d_truc/2 , h_pmllat,h_pmlbot};
  Rectangle(7)   = {-(h_pmllat+space_pml_x+w_guide/2.) ,                     -h_subs-h_low, -d_truc/2 , h_pmllat,h_subs};
  Rectangle(8)   = {-(h_pmllat+space_pml_x+w_guide/2.) ,                            -h_low, -d_truc/2 , h_pmllat,h_low};
  Rectangle(9)  = {-(h_pmllat+space_pml_x+w_guide/2.) ,                                 0, -d_truc/2 , h_pmllat,(h_guide+h_above+h_truc)};
  Rectangle(10)  = {-(h_pmllat+space_pml_x+w_guide/2.) , h_above+(h_guide+h_embed+h_truc) , -d_truc/2 , h_pmllat,h_pmltop};

  Rectangle(11)  = {(space_pml_x+w_guide/2.) ,            -h_pmlbot-h_subs-h_low, -d_truc/2 , h_pmllat,h_pmlbot};
  Rectangle(12)  = {(space_pml_x+w_guide/2.) ,                     -h_subs-h_low, -d_truc/2 , h_pmllat,h_subs};
  Rectangle(13)  = {(space_pml_x+w_guide/2.) ,                            -h_low, -d_truc/2 , h_pmllat,h_low};
  Rectangle(14)  = {(space_pml_x+w_guide/2.) ,                                 0, -d_truc/2 , h_pmllat,(h_guide+h_above+h_truc)};
  Rectangle(15)  = {(space_pml_x+w_guide/2.) , h_above+(h_guide+h_embed+h_truc) , -d_truc/2 , h_pmllat,h_pmltop};

  Rectangle(16)  = {-w_guide/2. ,    0, -d_truc/2 , w_guide,h_guide};

  Coherence;


  For k In {1:17}
    If (k!=4)
      f()=Extrude{0,0,(1)*d_truc}{ Surface{k}; };
      l[k]=f(0);
    Else
      l[k]=-1;
    EndIf
    // Printf("l%g l(k)%g",k,l(k));
  EndFor
  Cylinder(100) = {0,h_guide,0,0,h_truc,0,rl_truc};
  Dilate { { 0, h_guide+h_truc/2,0 }, { rt_truc/rl_truc, 1, 1 } } { Volume{100}; }
  Coherence;
  If(flag_sym==0)
    Periodic Surface {158,137,179,175,181,111,154,171,132,150,167,127,146,163,122,142} = {157,136,178,174,180,110,153,170,131,149,166,126,145,162,121,141} Translate {0,0,d_truc} ;
    Physical Volume("SUBS",1)             = {2};
    Physical Volume("LOW",2)              = {3};
    Physical Volume("GUIDE",3)            = {15};
    Physical Volume("SUPER",5)            = {101};
    Physical Volume("PMLSUBS_X",6)        = {11,6};
    Physical Volume("PMLSUBS_Y",7)        = {1};
    Physical Volume("PMLSUBS_XY",8)       = {10,5};
    Physical Volume("PMLLOW_X",9)         = {12,7};
    Physical Volume("PMLSUPER_X",11)      = {13,8};
    Physical Volume("PMLSUPER_Y",12)      = {4};
    Physical Volume("PMLSUPER_XY",13)     = {14,9};
    Physical Volume("TRUC",14)            = {100};
    Physical Surface("BLOCH_ZM",101)      = {157,136,178,174,180,110,153,170,131,149,166,126,145,162,121,141};
    Physical Surface("BLOCH_ZP",102)      = {158,137,179,175,181,111,154,171,132,150,167,127,146,163,122,142};
    Physical Surface("SURF_SYM_X0",110)   = {};
    Physical Surface("SURF_DIRI_OUT",120) = {155,134,177,176,172,168,164,160,159,117,138,140,144,148,152,156};
    Physical Point("PRINT_POINT",1000)    = {1};
  Else
    id_diff_block = newv;
    Block(id_diff_block) = {-(h_pmllat+space_pml_x+w_guide/2.+safe_bool_subs) ,
                  -h_pmlbot-h_subs-h_guide-h_low-safe_bool_subs,
                  -safe_bool_subs,
                    (h_pmllat+space_pml_x+w_guide/2.+safe_bool_subs),
                    -(-h_pmlbot-h_subs-h_guide-h_low-safe_bool_subs)+h_truc+h_pmltop+h_guide+h_pmllat+safe_bool_subs,
                    +N_truc*d_truc+2*safe_bool_subs};
    BooleanDifference{ Volume {:} ; Delete; }{ Volume{id_diff_block}; Delete; }

    Periodic Surface {182,163,187,167,171,191,199,208,175,179,196} = {181,162,185,166,170,189,198,206,174,178,194} Translate {0,0,d_truc} ;

    Physical Volume("SUBS",1)             = {2};
    Physical Volume("LOW",2)              = {3};
    Physical Volume("GUIDE",3)            = {15};
    Physical Volume("SUPER",5)            = {101};
    Physical Volume("PMLSUBS_X",6)        = {11};
    Physical Volume("PMLSUBS_Y",7)        = {1};
    Physical Volume("PMLSUBS_XY",8)       = {10};
    Physical Volume("PMLLOW_X",9)         = {12};
    Physical Volume("PMLSUPER_X",11)      = {13};
    Physical Volume("PMLSUPER_Y",12)      = {4};
    Physical Volume("PMLSUPER_XY",13)     = {14};
    Physical Volume("TRUC",14)            = {100};
    Physical Surface("BLOCH_ZM",101)      = {181,162,185,166,170,189,198,206,174,178,194};
    Physical Surface("BLOCH_ZP",102)      = {182,163,187,167,171,191,199,208,175,179,196};
    Physical Surface("SURF_SYM_X0",110)   = {183,186,190,200,205,207,195};
    Physical Surface("SURF_DIRI_OUT",120) = {197,177,176,172,168,164,160,159,180};
    Physical Point("PRINT_POINT",1000)    = {1};
  EndIf
  Characteristic Length{PointsOf{Physical Volume{11};}} = lc_PML_super;
  Characteristic Length{PointsOf{Physical Volume{12};}} = lc_PML_super;
  Characteristic Length{PointsOf{Physical Volume{13};}} = lc_PML_super;
  Characteristic Length{PointsOf{Physical Volume{10};}} = lc_PML_embed;
  Characteristic Length{PointsOf{Physical Volume{9};}}  = lc_PML_low;
  Characteristic Length{PointsOf{Physical Volume{6};}}  = lc_PML_subs;
  Characteristic Length{PointsOf{Physical Volume{7};}}  = lc_PML_subs;
  Characteristic Length{PointsOf{Physical Volume{8};}}  = lc_PML_subs;
  Characteristic Length{PointsOf{Physical Volume{5};}}  = lc_super;
  Characteristic Length{PointsOf{Physical Volume{4};}}  = lc_embed;
  Characteristic Length{PointsOf{Physical Volume{2};}}  = lc_low;
  Characteristic Length{PointsOf{Physical Volume{3};}}  = lc_guide;
  Characteristic Length{PointsOf{Physical Volume{1};}}  = lc_subs;
  Characteristic Length{PointsOf{Physical Volume{14};}} = lc_truc;
  If(flag_sym==0)
    Characteristic Length{213,214,215,216} = lc_low*1.5;
    Characteristic Length{191,192,198,202} = lc_low;
  EndIf
EndIf

// Mesh.RandomFactor=1e-7;

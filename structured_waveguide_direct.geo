Include "parameters_gmsh_getdp.dat";
Mesh.RandomFactor=1e-7;
If(flag_3D==0)
	paramaille = paramaille2D;
Else
	paramaille = paramaille3D;
EndIf

paramaille_subs     = lambda0/(paramaille*Sqrt[eps_subs]);
paramaille_pmllat   = lambda0/(paramaille*Sqrt[eps_subs]);
paramaille_pmltop   = lambda0/(paramaille*Sqrt[eps_super]);
paramaille_pmlbot   = lambda0/(paramaille*Sqrt[eps_subs]);
paramaille_subs     = lambda0/(paramaille*Sqrt[eps_subs]);
paramaille_low      = lambda0/(paramaille*Sqrt[eps_low]);
paramaille_guide    = lambda0/(paramaille*Sqrt[eps_guide]);
paramaille_super    = 0.8*lambda0/(paramaille*Sqrt[eps_super]);
paramaille_truc     = lambda0/(paramaille*Sqrt[Fabs[eps_truc_re]*3]);
paramaille_boxtrucs = paramaille_truc*3;		


Point(1)  = { h_pmllat+space_pml_x+w_guide/2 , -h_pmlbot-h_subs-h_guide-h_low, 0, paramaille_pmlbot};
Point(2)  = { h_pmllat+space_pml_x+w_guide/2 ,          -h_subs-h_guide-h_low, 0, paramaille_subs};
Point(3)  = { h_pmllat+space_pml_x+w_guide/2 ,              -h_guide-h_low, 0, paramaille_subs};
Point(4)  = { h_pmllat+space_pml_x+w_guide/2 ,            -h_guide, 0, paramaille_low};
Point(5)  = { h_pmllat+space_pml_x+w_guide/2 ,           h_above+h_truc, 0, paramaille_super};
Point(6)  = { h_pmllat+space_pml_x+w_guide/2 ,  h_pmltop+h_above+h_truc, 0, paramaille_super};
Point(7)  = {          space_pml_x+w_guide/2 , -h_pmlbot-h_subs-h_guide-h_low, 0, paramaille_pmlbot};
Point(8)  = {          space_pml_x+w_guide/2 ,          -h_subs-h_guide-h_low, 0, paramaille_subs};
Point(9)  = {          space_pml_x+w_guide/2 ,               -h_guide-h_low, 0, paramaille_subs};
Point(10) = {          space_pml_x+w_guide/2 ,            -h_guide, 0, paramaille_low};
Point(11) = {          space_pml_x+w_guide/2 ,           h_above+h_truc, 0, paramaille_super};
Point(12) = {          space_pml_x+w_guide/2 ,  h_pmltop+h_above+h_truc, 0, paramaille_super};
Point(13) = { w_guide/2                      ,  																0, 0, paramaille_guide};
Point(14) = { w_guide/2                      ,  															-h_guide, 0, paramaille_guide};

If(flag_sym==0)
	If(flag_3D==0)
		Point(15) = {-h_pmllat-space_pml_x-w_guide/2 , -h_pmlbot-h_subs-h_guide-h_low, 0, paramaille_pmlbot};
		Point(16) = {-h_pmllat-space_pml_x-w_guide/2 ,          -h_subs-h_guide-h_low, 0, paramaille_subs};
		Point(17) = {-h_pmllat-space_pml_x-w_guide/2 ,              -h_guide-h_low, 0, paramaille_subs};
		Point(18) = {-h_pmllat-space_pml_x-w_guide/2 ,            -h_guide, 0, paramaille_low};
		Point(19) = {-h_pmllat-space_pml_x-w_guide/2 ,           h_above+h_truc+h_depo, 0, paramaille_super};
		Point(20) = {-h_pmllat-space_pml_x-w_guide/2 ,  h_pmltop+h_above+h_truc+h_depo, 0, paramaille_super};
		Point(21) = {         -space_pml_x-w_guide/2 , -h_pmlbot-h_subs-h_guide-h_low, 0, paramaille_pmlbot};
		Point(22) = {         -space_pml_x-w_guide/2 ,          -h_subs-h_guide-h_low, 0, paramaille_subs};
		Point(23) = {         -space_pml_x-w_guide/2 ,               -h_guide-h_low, 0, paramaille_subs};
		Point(24) = {         -space_pml_x-w_guide/2 ,            -h_guide, 0, paramaille_low};
		Point(25) = {         -space_pml_x-w_guide/2 ,           h_above+h_truc+h_depo, 0, paramaille_super};
		Point(26) = {         -space_pml_x-w_guide/2 ,  h_pmltop+h_above+h_truc+h_depo, 0, paramaille_super};
		Point(27) = {-w_guide/2                      ,  																0, 0, paramaille_guide};
		Point(28) = {-w_guide/2                      ,  															-h_guide, 0, paramaille_guide};
		Line(1) = {15, 21};
		Line(2) = {21, 7};
		Line(3) = {7, 1};
		Line(4) = {1, 2};
		Line(5) = {2, 8};
		Line(6) = {8, 22};
		Line(7) = {22, 16};
		Line(8) = {16, 17};
		Line(9) = {17, 23};
		Line(10) = {23, 9};
		Line(11) = {9, 3};
		Line(12) = {3, 4};
		Line(13) = {4, 10};
		Line(14) = {10, 14};
		Line(15) = {14, 28};
		Line(16) = {28, 24};
		Line(17) = {24, 18};
		Line(18) = {18, 19};
		Line(19) = {19, 25};
		Line(20) = {25, 11};
		Line(21) = {11, 5};
		Line(22) = {5, 6};
		Line(23) = {6, 12};
		Line(24) = {12, 26};
		Line(25) = {26, 20};
		Line(26) = {20, 19};
		Line(27) = {18, 17};
		Line(28) = {16, 15};
		Line(29) = {21, 22};
		Line(30) = {22, 23};
		Line(31) = {23, 24};
		Line(32) = {24, 25};
		Line(33) = {25, 26};
		Line(34) = {7, 8};
		Line(35) = {8, 9};
		Line(36) = {9, 10};
		Line(37) = {10, 11};
		Line(38) = {11, 12};
		Line(39) = {5, 4};
		Line(40) = {3, 2};
		Line(41) = {14, 13};
		Line(42) = {13, 27};
		Line(43) = {27, 28};
		Line Loop(44) = {26, 19, 33, 25};
		Plane Surface(45) = {44};
		Line Loop(46) = {33, -24, -38, -20};
		Plane Surface(47) = {46};
		Line Loop(48) = {38, -23, -22, -21};
		Plane Surface(49) = {48};
		Line Loop(50) = {18, 19, -32, 17};
		Plane Surface(51) = {50};
		Line Loop(52) = {32, 20, -37, 14, 41, 42, 43, 16};
		Plane Surface(53) = {52};
		Line Loop(54) = {43, -15, 41, 42};
		Plane Surface(55) = {54};
		Line Loop(56) = {13, 37, 21, 39};
		Plane Surface(57) = {56};
		Line Loop(58) = {17, 27, 9, 31};
		Plane Surface(59) = {58};
		Line Loop(60) = {31, -16, -15, -14, -36, -10};
		Plane Surface(61) = {60};
		Line Loop(62) = {36, -13, -12, -11};
		Plane Surface(63) = {62};
		Line Loop(64) = {9, -30, 7, 8};
		Plane Surface(65) = {64};
		Line Loop(66) = {30, 10, -35, 6};
		Plane Surface(67) = {66};
		Line Loop(68) = {35, 11, 40, 5};
		Plane Surface(69) = {68};
		Line Loop(70) = {28, 1, 29, 7};
		Plane Surface(71) = {70};
		Line Loop(72) = {29, -6, -34, -2};
		Plane Surface(73) = {72};
		Line Loop(74) = {34, -5, -4, -3};
		Plane Surface(75) = {74};
	
		Physical Surface("SUBS")        = {67};
		Physical Surface("LOW")         = {61};
		Physical Surface("GUIDE")       = {55};
		Physical Surface("SUPER")       = {53};
		Physical Surface("PMLSUBS_X")   = {69,65};
		Physical Surface("PMLSUBS_Y")   = {73};
		Physical Surface("PMLSUBS_XY")  = {75,71};
		Physical Surface("PMLLOW_X")    = {63,59};
		Physical Surface("PMLSUPER_X")  = {51,57};
		Physical Surface("PMLSUPER_Y")  = {47};
		Physical Surface("PMLSUPER_XY") = {45,49};
		Physical Line("LINE_SYM_X0")    = {25,24,23,1,2,3};
		Physical Line("LINE_DIRICHLET_OUTER") = {28,8,27,18,26,22,39,12,40,4};
		Physical Point("PT_GUIDE_MID_BOT")    = {14};
	EndIf
EndIf

If(flag_sym==1)
	Point(15) = {0. , -h_pmlbot-h_subs-h_guide-h_low, 0, paramaille_pmlbot};
	Point(16) = {0. ,          -h_subs-h_guide-h_low, 0, paramaille_subs};
	Point(17) = {0. ,               0.-h_guide-h_low, 0, paramaille_subs};
	Point(18) = {0. ,                -h_guide, 0, paramaille_low};
	Point(19) = {0. ,    0, 0, paramaille_guide};
	Point(20) = {0. ,           h_above+h_truc  , 0, paramaille_super};
	Point(21) = {0. ,  h_pmltop+h_above+h_truc  , 0, paramaille_super};
EndIf

If(flag_sym==1)
	Line(1) = {1, 2};
	Line(2) = {2, 3};
	Line(3) = {3, 4};
	Line(4) = {4, 5};
	Line(5) = {5, 6};
	Line(6) = {7, 8};
	Line(7) = {8, 9};
	Line(8) = {9, 10};
	Line(9) = {10, 11};
	Line(10) = {11, 12};
	Line(11) = {15, 16};
	Line(12) = {16, 17};
	Line(13) = {17, 18};
	Line(14) = {18, 19};
	Line(15) = {19, 20};
	Line(16) = {20, 21};
	Line(17) = {14, 13};
	Line(18) = {15, 7};
	Line(19) = {7, 1};
	Line(20) = {16, 8};
	Line(21) = {8, 2};
	Line(22) = {17, 9};
	Line(23) = {9, 3};
	Line(24) = {18, 14};
	Line(25) = {14, 10};
	Line(26) = {10, 4};
	Line(27) = {19, 13};
	Line(28) = {20, 11};
	Line(29) = {11, 5};
	Line(30) = {21, 12};
	Line(31) = {12, 6};
	Line Loop(32) = {11, 20, -6, -18};
	Plane Surface(33) = {32};
	Line Loop(34) = {6, 21, -1, -19};
	Plane Surface(35) = {34};
	Line Loop(36) = {21, 2, -23, -7};
	Plane Surface(37) = {36};
	Line Loop(38) = {20, 7, -22, -12};
	Plane Surface(39) = {38};
	Line Loop(40) = {22, 8, -25, -24, -13};
	Plane Surface(41) = {40};
	Line Loop(42) = {8, 26, -3, -23};
	Plane Surface(43) = {42};
	Line Loop(44) = {9, 29, -4, -26};
	Plane Surface(45) = {44};
	Line Loop(46) = {24, 17, -27, -14};
	Plane Surface(47) = {46};
	Line Loop(48) = {25, 9, -28, -15, 27, -17};
	Plane Surface(49) = {48};
	Line Loop(50) = {28, 10, -30, -16};
	Plane Surface(51) = {50};
	Line Loop(52) = {10, 31, -5, -29};
	Plane Surface(53) = {52};
	If(flag_3D==0)
			Physical Surface("SUBS")        = {39};
			Physical Surface("LOW")         = {41};
			Physical Surface("GUIDE")       = {47};
			Physical Surface("SUPER")       = {49};
			Physical Surface("PMLSUBS_X")   = {37};
			Physical Surface("PMLSUBS_Y")   = {33};
			Physical Surface("PMLSUBS_XY")  = {35};
			Physical Surface("PMLLOW_X")    = {43};
			Physical Surface("PMLSUPER_X")  = {45};
			Physical Surface("PMLSUPER_Y")  = {51};
			Physical Surface("PMLSUPER_XY") = {53};
			Physical Line("LINE_SYM_X0")    = {11,12,13,14,15,16};
			Physical Line("LINE_DIRICHLET_OUTER") = {18,19,1,2,3,4,5,-31,-30}; //Gillesadd
			Physical Point("PT_GUIDE_MID_BOT")    = {18}; //Gillesadd
	EndIf
		
	If(flag_3D==1)
		Extrude {0, 0, h_pmlzm} {
		  Surface{33, 35, 37, 39, 43, 41, 45, 49, 47, 53, 51};
		}
		Extrude {0, 0, N_truc*d_truc+space_pml_zm+space_pml_zp} {
		  Surface{75, 97, 119, 141, 190, 163, 212, 266, 244, 310, 288};
		}
		Extrude {0, 0, h_pmlzp} {
		  Surface{354, 332, 376, 398, 447, 425, 469, 491, 523, 545, 567};
		}
		Delete {
		Volume{20, 19}; Surface{514, 486, 490}; Line{485};
		}
		centertrucs_x = 0;
		centertrucs_z = h_pmlzm+0.5*(N_truc*d_truc)+space_pml_zm;
		center1sttruc_x = 0;
		center1sttruc_z = h_pmlzm+0.5*d_truc+space_pml_zm;
		pt_boxtrucs = newp;
		Point(pt_boxtrucs  ) = { 0          , 0., centertrucs_z-0.5*(N_truc*d_truc)          , paramaille_boxtrucs};
		Point(pt_boxtrucs+1) = { w_box_trucs, 0., centertrucs_z+0.5*(N_truc*d_truc)          , paramaille_boxtrucs};
		Point(pt_boxtrucs+2) = { w_box_trucs, 0., centertrucs_z-0.5*(N_truc*d_truc)          , paramaille_boxtrucs};
		Point(pt_boxtrucs+3) = { 0          , 0., centertrucs_z+0.5*(N_truc*d_truc)          , paramaille_boxtrucs};
		Point(pt_boxtrucs+4) = { 0          , h_box_trucs, centertrucs_z-0.5*(N_truc*d_truc) , paramaille_boxtrucs};
		Point(pt_boxtrucs+5) = { w_box_trucs, h_box_trucs, centertrucs_z+0.5*(N_truc*d_truc) , paramaille_boxtrucs};
		Point(pt_boxtrucs+6) = { w_box_trucs, h_box_trucs, centertrucs_z-0.5*(N_truc*d_truc) , paramaille_boxtrucs};
		Point(pt_boxtrucs+7) = { 0          , h_box_trucs, centertrucs_z+0.5*(N_truc*d_truc) , paramaille_boxtrucs};
		
		Line(825) = {101, 318};
		Line(826) = {318, 320};
		Line(827) = {320, 324};
		Line(828) = {324, 322};
		Line(829) = {322, 318};
		Line(830) = {320, 319};
		Line(831) = {319, 323};
		Line(832) = {323, 325};
		Line(833) = {325, 321};
		Line(834) = {321, 319};
		Line(835) = {322, 325};
		Line(836) = {324, 323};
		Line(845) = {321, 197};			
		Line Loop(837) = {829, 826, 827, 828};
		Plane Surface(838) = {837};
		Line Loop(839) = {828, 835, -832, -836};
		Plane Surface(840) = {839};
		Line Loop(841) = {836, -831, -830, 827};
		Plane Surface(842) = {841};
		Line Loop(843) = {831, 832, 833, 834};
		Plane Surface(844) = {843};
		Line Loop(846) = {218, 481, 473, -845, 834, -830, -826, -825};
		Plane Surface(847) = {846};
		Line Loop(848) = {217, 825, -829, 835, 833, 845, -496, -509};
		Plane Surface(849) = {848};
		pt_truc = 1000;
		dk1 = 100;
		For k1 In {0:N_truc-1:1}
			Point(  pt_truc   +k1*dk1) = { center1sttruc_x        , 0., k1*d_truc+center1sttruc_z         , paramaille_truc};
			Point(  pt_truc+ 1+k1*dk1) = { center1sttruc_x        , 0., k1*d_truc+center1sttruc_z-rl_truc , paramaille_truc};
			Point(  pt_truc+ 2+k1*dk1) = { center1sttruc_x+rt_truc, 0., k1*d_truc+center1sttruc_z         , paramaille_truc};
			Point(  pt_truc+ 3+k1*dk1) = { center1sttruc_x        , 0., k1*d_truc+center1sttruc_z+rl_truc , paramaille_truc};
			Ellipse(pt_truc+ 4+k1*dk1) = {pt_truc+3+k1*dk1, pt_truc+k1*dk1, pt_truc+2+k1*dk1, pt_truc+2+k1*dk1};
			Ellipse(pt_truc+ 5+k1*dk1) = {pt_truc+2+k1*dk1, pt_truc+k1*dk1, pt_truc+1+k1*dk1, pt_truc+1+k1*dk1};
			Line(   pt_truc+ 6+k1*dk1) = {pt_truc+1+k1*dk1, pt_truc+3+k1*dk1};
			Point(  pt_truc+10+k1*dk1) = { center1sttruc_x        , h_truc, k1*d_truc+center1sttruc_z         , paramaille_truc};
			Point(  pt_truc+11+k1*dk1) = { center1sttruc_x        , h_truc, k1*d_truc+center1sttruc_z-rl_truc , paramaille_truc};
			Point(  pt_truc+12+k1*dk1) = { center1sttruc_x+rt_truc, h_truc, k1*d_truc+center1sttruc_z         , paramaille_truc};
			Point(  pt_truc+13+k1*dk1) = { center1sttruc_x        , h_truc, k1*d_truc+center1sttruc_z+rl_truc , paramaille_truc};
			Ellipse(pt_truc+14+k1*dk1) = {pt_truc+13+k1*dk1, pt_truc+10+k1*dk1, pt_truc+12+k1*dk1, pt_truc+12+k1*dk1};
			Ellipse(pt_truc+15+k1*dk1) = {pt_truc+12+k1*dk1, pt_truc+10+k1*dk1, pt_truc+11+k1*dk1, pt_truc+11+k1*dk1};
			Line(   pt_truc+16+k1*dk1) = {pt_truc+11+k1*dk1, pt_truc+13+k1*dk1};
			Line(   pt_truc+20+k1*dk1) = {pt_truc+ 1+k1*dk1, pt_truc+11+k1*dk1};
			Line(   pt_truc+21+k1*dk1) = {pt_truc+ 2+k1*dk1, pt_truc+12+k1*dk1};
			Line(   pt_truc+22+k1*dk1) = {pt_truc+ 3+k1*dk1, pt_truc+13+k1*dk1};
			If (k1!=0)
				Line(pt_truc+25+k1*dk1)={pt_truc+3+(k1-1)*dk1,pt_truc+1+k1*dk1};
			EndIf
			Line Loop(pt_truc+30+k1*dk1) = {pt_truc+k1*dk1+20, pt_truc+k1*dk1+16,-(pt_truc+k1*dk1+22), -(pt_truc+k1*dk1+6)};Plane Surface(pt_truc+31+k1*dk1) = {pt_truc+30+k1*dk1};
			Line Loop(pt_truc+32+k1*dk1) = {pt_truc+k1*dk1+16, pt_truc+k1*dk1+14,  pt_truc+k1*dk1+15};Plane Surface(pt_truc+33+k1*dk1) = {pt_truc+32+k1*dk1};
			Line Loop(pt_truc+34+k1*dk1) = {pt_truc+k1*dk1+6, pt_truc+k1*dk1+4, pt_truc+k1*dk1+5};Plane Surface(pt_truc+35+k1*dk1) = {pt_truc+34+k1*dk1};
			Line Loop(pt_truc+36+k1*dk1) = {pt_truc+k1*dk1+20, -(pt_truc+k1*dk1+15), -(pt_truc+k1*dk1+21), pt_truc+k1*dk1+5};Ruled Surface(pt_truc+37+k1*dk1) = {pt_truc+36+k1*dk1};
			Line Loop(pt_truc+38+k1*dk1) = {pt_truc+k1*dk1+21, -(pt_truc+k1*dk1+14), -(pt_truc+k1*dk1+22), pt_truc+k1*dk1+4};Ruled Surface(pt_truc+39+k1*dk1) = {pt_truc+38+k1*dk1};
			Surface Loop(pt_truc+39+k1*dk1) = {pt_truc+31+k1*dk1, pt_truc+37+k1*dk1, pt_truc+33+k1*dk1, pt_truc+39+k1*dk1, pt_truc+35+k1*dk1};
			Volume(pt_truc+40+k1*dk1) = {pt_truc+39+k1*dk1};				
		EndFor
		Line(pt_truc+(k1+1)*dk1  ) = {318,pt_truc+1};
		Line(pt_truc+(k1+1)*dk1+1) = {pt_truc+3+(k1-1)*dk1,321};

		// // surface guide Oyz
		For k2 In {0:N_truc-1:1}
			ll_sguideOxy[0+2*k2+0] = pt_truc+k2*dk1+25;
			ll_sguideOxy[0+2*k2+1] = pt_truc+k2*dk1+6;
		EndFor
		ll_sguideOxy[0]  = pt_truc+(k1+1)*dk1;
		ll_sguideOxy[2*k2+0]  = pt_truc+(k1+1)*dk1+1;
		ll_sguideOxy[2*k2+1]  = 845;
		ll_sguideOxy[2*k2+2]  = 474;
		ll_sguideOxy[2*k2+3]  = -419;
		ll_sguideOxy[2*k2+4]  = -249;
		ll_sguideOxy[2*k2+5]  = 825;
		// Printf("ll_sguideOxy");
		// For k3 In {0:13}
		// 	Printf("%g",ll_sguideOxy[k3]);
		// EndFor
		Line Loop(pt_truc+(k1+1)*dk1+1) = ll_sguideOxy[];
		Plane Surface(pt_truc+(k1+1)*dk1+2) = {pt_truc+(k1+1)*dk1+1};

		// surface box Oyz
		ll_sboxOxy[0] = pt_truc+(k1+1)*dk1;
					For k2 In {0:N_truc-1:1}
			ll_sboxOxy[1+4*k2+0] =  pt_truc+20+k2*dk1;
			ll_sboxOxy[1+4*k2+1] =  pt_truc+16+k2*dk1;
			ll_sboxOxy[1+4*k2+2] =-(pt_truc+22+k2*dk1);
			ll_sboxOxy[1+4*k2+3] =  pt_truc+25+(k2+1)*dk1;
		EndFor
		ll_sboxOxy[1+4*(N_truc-1)+3] = pt_truc+(k1+1)*dk1+1;
		ll_sboxOxy[1+4*(N_truc-1)+4] = -833;
		ll_sboxOxy[1+4*(N_truc-1)+5] = -835;
		ll_sboxOxy[1+4*(N_truc-1)+6] =  829;
		// Printf("ll_sboxOxy");
		// For k3 In {0:15}
		// 	Printf("%g",ll_sboxOxy[k3]);
		// EndFor
		Line Loop(pt_truc+(k1+1)*dk1+3) = ll_sboxOxy[];
		Plane Surface(pt_truc+(k1+1)*dk1+4) = {pt_truc+(k1+1)*dk1+3};

		// surface box bot Oxz
		ll_sboxOxz[0] =  826;
		ll_sboxOxz[1] =  830;
		ll_sboxOxz[2] = -834;
		ll_sboxOxz[3] = -(pt_truc+(k1+1)*dk1+1);
		For k2 In {1	:N_truc:1}
			ll_sboxOxz[4+3*(k2-1)+0] =   pt_truc+(N_truc-k2)*dk1+4;
			ll_sboxOxz[4+3*(k2-1)+1] =   pt_truc+(N_truc-k2)*dk1+5;
			ll_sboxOxz[4+3*(k2-1)+2] = -(pt_truc+(N_truc-k2)*dk1+25);
		EndFor
		ll_sboxOxz[4+3*(N_truc-1)+2] = -(pt_truc+(k1+1)*dk1);
		Line Loop(pt_truc+(k1+1)*dk1+5) = ll_sboxOxz[];
		Plane Surface(pt_truc+(k1+1)*dk1+6) = {pt_truc+(k1+1)*dk1+5};

		// Volume Guide
		sl_guide[0] = 482;
		sl_guide[1] = 420;
		sl_guide[2] = pt_truc+(k1+1)*dk1+2;
		sl_guide[3] = 266;
		sl_guide[4] = 491;
		sl_guide[5] = 847;
		sl_guide[6] = pt_truc+(k1+1)*dk1+6;
		For k2 In {0:N_truc-1:1}
			sl_guide[7+k2] = pt_truc+(N_truc-1-k2)*dk1+35;
		EndFor
		Surface Loop(pt_truc+(k1+1)*dk1+7) = sl_guide[];
		Volume(pt_truc+(k1+1)*dk1+8) = {pt_truc+(k1+1)*dk1+7};

		// Volume Box
		sl_box[0] = 840;
		sl_box[1] = 844;
		sl_box[2] = 842;
		sl_box[3] = pt_truc+(k1+1)*dk1+6;
		sl_box[4] = 838;
		sl_box[5] = pt_truc+(k1+1)*dk1+4;
		For k2 In {0:N_truc-1:1}
			sl_box[5+3*k2+1] = pt_truc+(N_truc-1-k2)*dk1+33;
			sl_box[5+3*k2+2] = pt_truc+(N_truc-1-k2)*dk1+39;
			sl_box[5+3*k2+3] = pt_truc+(N_truc-1-k2)*dk1+37;
		EndFor
		Surface Loop(pt_truc+(k1+1)*dk1+9) = sl_box[];
		Volume(pt_truc+(k1+1)*dk1+10) = {pt_truc+(k1+1)*dk1+9};

		// Volume super
		Surface Loop(pt_truc+(k1+1)*dk1+11) = {840, 844, 842, 838, 847, 244, 510, 456, 523, 416, 482, 849};
		Volume(pt_truc+(k1+1)*dk1+12) = {pt_truc+(k1+1)*dk1+11};
			
		// phys loop truc
		For k2 In {0:N_truc-1:1}
			physl_truc[k2] = pt_truc+40+k2*dk1;
		EndFor
		// phys loop sym_x0
		physl_sym_x0={62,319,598,140,397,654,189,424,703,309,544,801,265,pt_truc+(k1+1)*dk1+2,747,235,849,pt_truc+(k1+1)*dk1+4,771};
		For k2 In {0:N_truc-1:1}
			physl_sym_x0[k2+19]=pt_truc+k2*dk1+31;
		EndFor


	// Physical volumes
	Physical Volume("SUBS")          = {15};
	Physical Volume("LOW")           = {16};
	Physical Volume("GUIDE")         = {pt_truc+(k1+1)*dk1+8};
	Physical Volume("SUPER")         = {pt_truc+(k1+1)*dk1+12};
	Physical Volume("TRUC")          = physl_truc[];
	Physical Volume("PMLSUBS_X")     = {14};
	Physical Volume("PMLSUBS_Y")     = {12};
	Physical Volume("PMLSUBS_Z")     = {4,26};
	Physical Volume("PMLSUBS_XY")    = {13};
	Physical Volume("PMLSUBS_YZ")    = {1,24};
	Physical Volume("PMLSUBS_ZX")    = {3,25};
	Physical Volume("PMLSUBS_XYZ")   = {2,23};
	Physical Volume("PMLLOW_X")      = {17};
	Physical Volume("PMLLOW_Z")      = {6,28};
	Physical Volume("PMLLOW_XZ")     = {5,27};
	Physical Volume("PMLGUIDE_Z")    = {9,30};
	Physical Volume("PMLSUPER_X")    = {18};
	Physical Volume("PMLSUPER_Y")    = {21};
	Physical Volume("PMLSUPER_Z")    = {8,31};
	Physical Volume("PMLSUPER_XY")   = {22};
	Physical Volume("PMLSUPER_YZ")   = {11,32};
	Physical Volume("PMLSUPER_ZX")   = {7,29};
	Physical Volume("PMLSUPER_XYZ")  = {10,33};
	Physical Surface("SURF_SYM_X0")  = physl_sym_x0[];
	Physical Volume("BOX")          = {pt_truc+(k1+1)*dk1+10};
	Physical Surface("SURF_INJ")    = {33,35,37,39,41,43,45,47,49,51,53};
EndIf
Mesh.Optimize=1;
// Mesh.MshFileVersion = 2;

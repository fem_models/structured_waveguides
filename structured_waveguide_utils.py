import numpy as np
import matplotlib
matplotlib.use('Agg')
matplotlib.rc('figure', **{'autolayout': True})
from matplotlib.colors import LogNorm
import pylab as pl
pi=np.pi

def convert_getdp_IO_realscalar(fname_in,fnames_out,nx,ny):
    f_re = open(fnames_out+'_re.dat', 'w+')
    f_im = open(fnames_out+'_im.dat', 'w+')
    u_grid_data = np.loadtxt(fname_in)
    x_export2D = u_grid_data[:,2].reshape((nx,ny))
    y_export2D = u_grid_data[:,3].reshape((nx,ny))
    u_grid_re  = u_grid_data[:,8].reshape((nx,ny))
    u_grid_im  = u_grid_data[:,9].reshape((nx,ny))
    np.savetxt(f_re, np.array([nx,ny],dtype=int),fmt='%g', newline=' ')
    np.savetxt(f_im, np.array([nx,ny],dtype=int),fmt='%g', newline=' ')
    f_re.write('\n')
    f_im.write('\n')
    np.savetxt(f_re, x_export2D[:,0], newline=' ')
    np.savetxt(f_im, x_export2D[:,0], newline=' ')
    f_re.write('\n')
    f_im.write('\n')
    np.savetxt(f_re, y_export2D[0,:], newline=' ')
    np.savetxt(f_im, y_export2D[0,:], newline=' ')
    f_re.write('\n')
    f_im.write('\n')
    np.savetxt(f_re, u_grid_re.transpose())
    np.savetxt(f_im, u_grid_im.transpose())
    f_re.close()
    f_im.close()

def dtrap_poy(fname_in,nx,ny):
    poy_data = np.loadtxt(fname_in)
    x_export2D = poy_data[:,2].reshape((nx,ny))
    y_export2D = poy_data[:,3].reshape((nx,ny))
    poy_y_grid_re  = poy_data[:,10].reshape((nx,ny))
    # xx_interp,yy_interp = np.meshgrid(x_export2D[:,0], y_export2D[0,:], sparse=False, indexing='ij')
    # pl.figure();pl.contourf(xx_interp,yy_interp,poy_y_grid_re,30);pl.title(fname_in);pl.axis('equal');pl.savefig(fname_in+'.pdf');pl.close('all')
    temp=np.trapz(poy_y_grid_re,x_export2D[:,0],axis=0)
    return np.trapz(temp,y_export2D[0,:]) #[x_export2D,y_export2D,poy_y_grid_re] #

def convert_getdp_IO_complexvector(fname_in,fnames_out,disp_name,nx,ny,eig_nb,flag_plot):
    u_grid_data = np.loadtxt(fname_in)
    x_export2D = u_grid_data[:,2].reshape((nx,ny))
    y_export2D = u_grid_data[:,3].reshape((nx,ny))
    u_grid_x_re  = u_grid_data[:, 8+eig_nb*6].reshape((nx,ny))
    u_grid_y_re  = u_grid_data[:, 9+eig_nb*6].reshape((nx,ny))
    u_grid_z_re  = u_grid_data[:,10+eig_nb*6].reshape((nx,ny))
    u_grid_x_im  = u_grid_data[:,11+eig_nb*6].reshape((nx,ny))
    u_grid_y_im  = u_grid_data[:,12+eig_nb*6].reshape((nx,ny))
    u_grid_z_im  = u_grid_data[:,13+eig_nb*6].reshape((nx,ny))
    f_x_re = open(fnames_out+'_x_re.dat', 'wb+')
    f_y_re = open(fnames_out+'_y_re.dat', 'wb+')
    f_z_re = open(fnames_out+'_z_re.dat', 'wb+')
    f_x_im = open(fnames_out+'_x_im.dat', 'wb+')
    f_y_im = open(fnames_out+'_y_im.dat', 'wb+')
    f_z_im = open(fnames_out+'_z_im.dat', 'wb+')
    np.savetxt(f_x_re, [int(nx),int(ny)],fmt='%d', newline=' ')
    np.savetxt(f_y_re, [int(nx),int(ny)],fmt='%d', newline=' ')
    np.savetxt(f_z_re, [int(nx),int(ny)],fmt='%d', newline=' ')
    np.savetxt(f_x_im, [int(nx),int(ny)],fmt='%d', newline=' ')
    np.savetxt(f_y_im, [int(nx),int(ny)],fmt='%d', newline=' ')
    np.savetxt(f_z_im, [int(nx),int(ny)],fmt='%d', newline=' ')
    f_x_re.write(b'\n')
    f_y_re.write(b'\n')
    f_z_re.write(b'\n')
    f_x_im.write(b'\n')
    f_y_im.write(b'\n')
    f_z_im.write(b'\n')
    np.savetxt(f_x_re, x_export2D[:,0], newline=' ')
    np.savetxt(f_y_re, x_export2D[:,0], newline=' ')
    np.savetxt(f_z_re, x_export2D[:,0], newline=' ')
    np.savetxt(f_x_im, x_export2D[:,0], newline=' ')
    np.savetxt(f_y_im, x_export2D[:,0], newline=' ')
    np.savetxt(f_z_im, x_export2D[:,0], newline=' ')
    f_x_re.write(b'\n')
    f_y_re.write(b'\n')
    f_z_re.write(b'\n')
    f_x_im.write(b'\n')
    f_y_im.write(b'\n')
    f_z_im.write(b'\n')
    np.savetxt(f_x_re, y_export2D[0,:], newline=' ')
    np.savetxt(f_y_re, y_export2D[0,:], newline=' ')
    np.savetxt(f_z_re, y_export2D[0,:], newline=' ')
    np.savetxt(f_x_im, y_export2D[0,:], newline=' ')
    np.savetxt(f_y_im, y_export2D[0,:], newline=' ')
    np.savetxt(f_z_im, y_export2D[0,:], newline=' ')
    f_x_re.write(b'\n')
    f_y_re.write(b'\n')
    f_z_re.write(b'\n')
    f_x_im.write(b'\n')
    f_y_im.write(b'\n')
    f_z_im.write(b'\n')
    np.savetxt(f_x_re, u_grid_x_re.transpose())
    np.savetxt(f_y_re, u_grid_y_re.transpose())
    np.savetxt(f_z_re, u_grid_z_re.transpose())
    np.savetxt(f_x_im, u_grid_x_im.transpose())
    np.savetxt(f_y_im, u_grid_y_im.transpose())
    np.savetxt(f_z_im, u_grid_z_im.transpose())
    f_x_re.close()
    f_y_re.close()
    f_z_re.close()
    f_x_im.close()
    f_y_im.close()
    f_z_im.close()
    if flag_plot:
        xx_interp,yy_interp = np.meshgrid(x_export2D[:,0], y_export2D[0,:], sparse=False, indexing='ij')
        fig, axes = pl.subplots(nrows=2, ncols=3, sharex=True, sharey=True,figsize=(15,9))
        imE_x_re=axes[0,0].contourf(xx_interp,yy_interp,u_grid_x_re,30);axes[0,0].set_title(r"$\mathrm{Re}\{"+disp_name+"x\}$");pl.colorbar(imE_x_re, ax = axes[0,0])
        imE_y_re=axes[0,1].contourf(xx_interp,yy_interp,u_grid_y_re,30);axes[0,1].set_title(r"$\mathrm{Re}\{"+disp_name+"y\}$");pl.colorbar(imE_y_re, ax = axes[0,1])
        imE_z_re=axes[0,2].contourf(xx_interp,yy_interp,u_grid_z_re,30);axes[0,2].set_title(r"$\mathrm{Re}\{"+disp_name+"z\}$");pl.colorbar(imE_z_re, ax = axes[0,2])
        imE_x_im=axes[1,0].contourf(xx_interp,yy_interp,u_grid_x_im,30);axes[1,0].set_title(r"$\mathrm{Im}\{"+disp_name+"x\}$");pl.colorbar(imE_x_im, ax = axes[1,0])
        imE_y_im=axes[1,1].contourf(xx_interp,yy_interp,u_grid_y_im,30);axes[1,1].set_title(r"$\mathrm{Im}\{"+disp_name+"y\}$");pl.colorbar(imE_y_im, ax = axes[1,1])
        imE_z_im=axes[1,2].contourf(xx_interp,yy_interp,u_grid_z_im,30);axes[1,2].set_title(r"$\mathrm{Im}\{"+disp_name+"z\}$");pl.colorbar(imE_z_im, ax = axes[1,2])
        pl.savefig(fnames_out+'_check.png')
        pl.close('all')

def get_all_eigenvectors_ongrid(fname_in,nx,ny,neig):
    u_grid_data = np.loadtxt(fname_in)
    x_export2D = u_grid_data[:,2].reshape((nx,ny))
    y_export2D = u_grid_data[:,3].reshape((nx,ny))
    field        = np.zeros((neig,3,nx,ny),dtype=complex)
    for k in range(neig):
        u_grid_x_re  = u_grid_data[:, 8+k*6].reshape((nx,ny))
        u_grid_y_re  = u_grid_data[:, 9+k*6].reshape((nx,ny))
        u_grid_z_re  = u_grid_data[:,10+k*6].reshape((nx,ny))
        u_grid_x_im  = u_grid_data[:,11+k*6].reshape((nx,ny))
        u_grid_y_im  = u_grid_data[:,12+k*6].reshape((nx,ny))
        u_grid_z_im  = u_grid_data[:,13+k*6].reshape((nx,ny))
        res          = {}
        field[k,0,:,:] = u_grid_x_re+1j*u_grid_x_im
        field[k,1,:,:] = u_grid_y_re+1j*u_grid_y_im
        field[k,2,:,:] = u_grid_z_re+1j*u_grid_z_im
    res['tab_x'] = x_export2D[:,0]
    res['tab_y'] = y_export2D[0,:]
    res['fields'] = field
    return res

def plot_cut_maps(fname_in,fnames_out,nx,nz,d,h,rl,rt,max_r,space_pml_z):
    u_grid_data = np.loadtxt(fname_in)
    x_export2D = u_grid_data[:,2].reshape((nx,nz))
    z_export3D = u_grid_data[:,4].reshape((nx,nz))
    u_grid_x_re  = u_grid_data[:, 8].reshape((nx,nz))
    u_grid_y_re  = u_grid_data[:, 9].reshape((nx,nz))
    u_grid_z_re  = u_grid_data[:,10].reshape((nx,nz))
    u_grid_x_im  = u_grid_data[:,11].reshape((nx,nz))
    u_grid_y_im  = u_grid_data[:,12].reshape((nx,nz))
    u_grid_z_im  = u_grid_data[:,13].reshape((nx,nz))
    u_grid_x     = u_grid_x_re + 1j*u_grid_x_im
    u_grid_y     = u_grid_y_re + 1j*u_grid_y_im
    u_grid_z     = u_grid_z_re + 1j*u_grid_z_im
    avoid_zero_log=1e-10
    xx_interp,zz_interp = np.meshgrid(x_export2D[:,0], z_export3D[0,:], sparse=False, indexing='ij')
    u_grid_x=u_grid_x[1::,:]
    u_grid_y=u_grid_y[1::,:]
    u_grid_z=u_grid_z[1::,:]
    xx_interp=xx_interp[1::,:]
    zz_interp=zz_interp[1::,:]
    fig, axes = pl.subplots(nrows=4, ncols=1, sharex=True, sharey=True,figsize=(8,15))
    V_n = np.sqrt(np.abs(u_grid_x)**2+np.abs(u_grid_y)**2+np.abs(u_grid_z)**2)
    V_x = np.abs(u_grid_x)
    V_y = np.abs(u_grid_y)
    V_z = np.abs(u_grid_z)
    im_normE=axes[0].pcolormesh(xx_interp,zz_interp-z_export3D[0,int(nz/2)],V_n,norm=LogNorm(vmin=V_n.min(), vmax=V_n.max()))
    im_modEx=axes[1].pcolormesh(xx_interp,zz_interp-z_export3D[0,int(nz/2)],V_x,norm=LogNorm(vmin=V_x.min(), vmax=V_x.max()))
    im_modEy=axes[2].pcolormesh(xx_interp,zz_interp-z_export3D[0,int(nz/2)],V_y,norm=LogNorm(vmin=V_y.min(), vmax=V_y.max()))
    im_modEz=axes[3].pcolormesh(xx_interp,zz_interp-z_export3D[0,int(nz/2)],V_z,norm=LogNorm(vmin=V_z.min(), vmax=V_z.max()))
    axes[0].set_title(r"$|\mathbf{E}|$")
    axes[1].set_title(r"$|E_x|$")
    axes[2].set_title(r"$|E_y|$")
    axes[3].set_title(r"$|E_z|$")
    for k in range(4):
        axes[k].set_xlabel(r"$x\,(\mu m)$")
        axes[k].set_ylabel(r"$z\,(\mu m)$")
        axes[k].plot(rt*np.cos(np.linspace(-pi/2,pi/2,200)),\
                         +rl*np.sin(np.linspace(-pi/2,pi/2,200)),\
                         'k-',lw=.5)
    pl.colorbar(im_normE, ax = axes[0]); axes[0].set_aspect('equal')
    pl.colorbar(im_modEx, ax = axes[1]); axes[1].set_aspect('equal')
    pl.colorbar(im_modEy, ax = axes[2]); axes[2].set_aspect('equal')
    pl.colorbar(im_modEz, ax = axes[3]); axes[3].set_aspect('equal')
    fig.suptitle("$max=%.2f - h=%.0fnm\,-\,d_l=%.3f\mu m\,-\,d_t=%.3f\mu m$"%((V_n.max(),h*1000,2.*rl,2.*rt)))
    pl.savefig(fnames_out+'_all_log_d%.0fnm_h%.0fnm_dl%.3fum_dt%.3fum.png'%((d*1000.,h*1000.,2.*rl,2.*rt)))
    pl.close('all')

def DumpAllToTxt(current_globals,filename):
    import types
    f = open(filename,'w')
    for key in current_globals.keys():
        cur_type = type(current_globals[key])
        cur_val  = current_globals[key]
        if key[0]!='_' \
        and cur_type!=types.FunctionType \
        and cur_type!=types.ModuleType:
            try:
                if isinstance(cur_val, float): f.write(key+' = %3.15e;\n'%(cur_val))
                if isinstance(cur_val, int  ): f.write(key+' = %d;\n'%(cur_val))
                if isinstance(cur_val, str  ): f.write(key+' = "%s";\n'%(cur_val))
                if isinstance(cur_val, complex):
                    f.write(key+'_re = %3.15e;\n'%(cur_val.real))
                    f.write(key+'_im = %3.15e;\n'%(cur_val.imag))
            except:
                print('discarded while dumping: {0}'.format(key))
    f.close()

def UpdateParam(str_varname,value,filename):
    f = open(filename,'a')
    if isinstance(value, float): f.write(str_varname+' = %3.15e;\n'%(value))
    if isinstance(value, int  ): f.write(str_varname+' = %d;\n'%(value))
    if isinstance(value, str  ): f.write(str_varname+' = "%s";\n'%(value))
    if isinstance(value, complex):
        f.write(str_varname+'_re = %3.15e;\n'%(value.real))
        f.write(str_varname+'_im = %3.15e;\n'%(value.imag))
    f.close()

def get_ndarray_bytesize(arr):
    return(arr.size*arr.itemsize)

def saveall(current_globals, filename, light=False,limit_MB=0.5):
    dic2save = {}
    for glob_key in sorted(iter(current_globals.keys())):
        glob_type=type(current_globals[glob_key])
        if (glob_type in (int, bool, float, complex, str, list, dict, 
                         np.float64, np.float128, np.float128, np.complex128, np.ndarray) 
        and glob_key[0]!='_'):
            if light and glob_type==np.ndarray:
                if get_ndarray_bytesize(current_globals[glob_key])*1e-6>limit_MB:
                    dic2save[glob_key] = 'discarded: Too heavy'
                else:
                    dic2save[glob_key] = current_globals[glob_key]
            else:
                dic2save[glob_key] = current_globals[glob_key]
    np.savez_compressed(filename,saved=dic2save)

def loadall(filename):
    res=np.load(filename,allow_pickle=True)
    return res.f.saved.item()

rm -rf run_results
rm -rf __pycache__
rm *.msh
rm *.dat
rm *.pre
rm *.res
rm *.png
rm *.pdf
rm *.npz
